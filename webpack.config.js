let webpack = require("webpack");
let path = require("path");
let FriendlyErrorsWebpackPlugin = require("friendly-errors-webpack-plugin");

function resolve(dir) {
	return path.resolve(__dirname, dir);
}

const PROXY = process.env.PROXY || "israel.bingo.local";
const PRODUCTION = +process.env.PROD === 1;
const ENTRY = process.env.ENTRY || "main";
const VueLoaderPlugin = require("vue-loader/lib/plugin");
function getPublicPath(port) {
	return `http://${PROXY}:${port}/`;
}

const config = {
	mode: PRODUCTION ? "production" : "development",
	entry: {},
	module: {
		rules: [
			{test: /\.js$/, exclude: /node_modules/, loader: "babel-loader"},
			{test: /\.vue/, loader: "vue-loader"},
			{test: /\.css$/, use: [{loader: "style-loader"}, {loader: "css-loader"}]},
			{test: /\.html$/, loader: "html-loader"},
			{test: /\.(woff|woff2)$/, loader: "url-loader?limit=10000&mimetype=application/font-woff"},
			{test: /\.ttf$/, loader: "file-loader"},
			{test: /\.eot$/, loader: "file-loader"},
			{test: /\.svg$/, loader: "file-loader"},
			{
				test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
				loader: "url-loader",
				options: {
					limit: 500,
					name: "images/[name].[hash:7].[ext]"
				}
			}

		]
	},
	resolve: {
		extensions: [".js", ".vue", ".json"],
		alias: {
			"vue$": "vue/dist/vue.esm.js",
			"dev": resolve("dev"),
			"@": resolve("public"),
			"assets": resolve("dev/assets")
		}
	},
	plugins: [
		new webpack.ProvidePlugin({
			// $: "jquery",
			// jQuery: "jquery",
			moment: "moment",
			swal: "sweetalert2"
		}),
		new webpack.DefinePlugin({
			"process.env": {
				"DEBUG": !PRODUCTION,
				NODE_ENV: PRODUCTION ? "'production'" : "'development'"
			}
		}),

		new VueLoaderPlugin()

	],
};

if (PRODUCTION) {
	config.entry = {prod: ["babel-polyfill", "./dev/main.js"]};
	config.output = {
		path: __dirname + "/public",
		filename: "js/[name].js",
		sourceMapFilename: "js/[name].js.map"
	};
	config.optimization = {
		splitChunks: {
			cacheGroups: {
				default: false,
				commons: {
					test: /[\\/]node_modules[\\/]/,
					name: "vendor",
					filename: "js/vendor.js",
					chunks: "all"
				}
			}
		}
	};
} else {
	config.entry[ENTRY] = ["babel-polyfill", `./dev/${ENTRY}.js`];
	config.output = {
		path: path.resolve("public/js/"),
		publicPath: getPublicPath(8080),
		filename: "js/[name].js",
		sourceMapFilename: "js/[name].js.map"
	};
	config.plugins.push(new webpack.HotModuleReplacementPlugin());
	config.plugins.push(new FriendlyErrorsWebpackPlugin());
	config.devServer = {
		hot: true,
		contentBase: path.join(__dirname, "public"),
		watchContentBase: true,
		host: PROXY,
		proxy: {
			"*": `http://${PROXY}/`
		},
		inline: true,
		overlay: true,
		open: true,
	};
	config.devtool = "inline-source-map";


}

module.exports = config;
