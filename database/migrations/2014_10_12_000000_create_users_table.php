<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$table->increments('id');
			$table->char('email',100)->unique();
			$table->string('name');
			$table->integer('role')->default(0);
			$table->string('password')->nullable();
			$table->string('address')->nullable();
			$table->string('leumi_place')->nullable();
			$table->string('phone_number')->nullable();
			$table->string('organization')->nullable();
			$table->string('OS_id')->nullable();
			$table->time('daily_reminder')->nullable();
			$table->time('study_reminder')->nullable();
			$table->string("image")->nullable();
			$table->rememberToken();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}
