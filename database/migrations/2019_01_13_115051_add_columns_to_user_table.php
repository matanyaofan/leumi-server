<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUserTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function (Blueprint $table) {
			$table->string("tz")->nullable();
			$table->string("city")->nullable();
			$table->string("street")->nullable();
			$table->string("home_number")->nullable();
			$table->string("ZIP_code")->nullable();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function (Blueprint $table) {
			$table->dropColumn("tz");
			$table->dropColumn("city");
			$table->dropColumn("street");
			$table->dropColumn("home_number");
			$table->dropColumn("ZIP_code");

		});
	}
}
