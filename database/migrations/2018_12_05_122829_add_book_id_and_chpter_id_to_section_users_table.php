<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBookIdAndChpterIdToSectionUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('section_users', function (Blueprint $table) {
			$table->integer('book_id')->after("user_id");
			$table->integer('chapter_id')->after("book_id");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('section_users', function (Blueprint $table) {
			//
		});
	}
}
