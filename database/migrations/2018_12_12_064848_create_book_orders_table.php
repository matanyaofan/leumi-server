<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookOrdersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('book_orders', function (Blueprint $table) {
			$table->increments('id');
			$table->integer("book_id");
			$table->integer("user_id")->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('users');
			$table->boolean("is_reach")->default(0);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('book_orders');
	}
}
