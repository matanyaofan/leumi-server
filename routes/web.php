<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'api', "namespace" => "Admin",], function () {
	Route::auth();
	Route::group(['middleware' => 'auth'], function () {
		Route::get("auth", "Auth\LoginController@auth");
		Route::resource('user', "UserController");
		Route::get("export/users", "UserController@export");
		Route::post("notification", "NotificationController@send");
        Route::resource('bookorder', "BookOrderController");
	});
});

use Illuminate\Support\Facades\Route;

Route::get('/users', "UserController@index");

Route::get('/', function () {
	return View('welcome');
});

Route::group(['prefix' => 'api/app/', "namespace" => "Api", "middleware" => \Barryvdh\Cors\HandleCors::class], function () {
	Route::get('init', "SectionController@initSections");
	Route::get('update/sections', "SectionController@updateSections");
	Route::get('delete/mistakes', "SectionUserController@deleteMistakeSections");
	Route::get('book/init', "BookController@initBooks");
	Route::get('test/best', "TestsController@getBestTable");
	Route::post('register', 'Auth\RegisterController@register');
	Route::get('registerTest', 'Auth\RegisterController@registerUserToTests');
	Route::post('login/google', "Auth\GoogleLoginController@googleLogin");
	Route::post('login', "Auth\LoginController@login");
	Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
	Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
	Route::post('password/reset', 'Auth\ResetPasswordController@reset');

	Route::group(["middleware" => \App\Http\Middleware\ApiAuth::class], function () {
		Route::resource('daily', "RssFeedController");
		Route::get('test/passed', "TestsController@isPassedTheTest");
		Route::resource('test', "TestsController");
		Route::post('user/image', "UserController@updateImage");
		Route::post('password/set', "Auth\RegisterController@setPassword");
		Route::resource('user', "UserController");
		Route::get('auth', "Auth\LoginController@auth");
		Route::delete('section/user/remove', "SectionUserController@deleteSectionFromUser");
		Route::post('section/user/remove/many', "SectionUserController@deleteManySectionsFromUser");
		Route::post('contact', "AppController@sendMail");
		Route::resource('section/user', "SectionUserController");
		Route::resource('section', "SectionController");
		Route::resource('book/order', "BookOrderController");
		Route::resource('book', "BookController");
		Route::resource('notification', "NotificationController");
		Route::get('logout', "Auth\LoginController@logout");
	});
});

Route::group(['prefix' => 'api/', "namespace" => "Api", "middleware" => \Barryvdh\Cors\HandleCors::class], function () {

	Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
	Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
	Route::post('password/reset', 'Auth\ResetPasswordController@reset');
});
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware("auth");


Route::get('{page}', function () {
	return View('welcome');
})->where(['page' => '^((?!api).)*$']);
