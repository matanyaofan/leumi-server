<?php

use App\BookOrder;
use Illuminate\Foundation\Inspiring;
use  App\Http\Controllers\Api\Auth\RegisterController;
use App\User;
/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('bookorder:clean', function () {
    return BookOrder::query()
        //TODO fix the database!
        ->whereNotIn("user_id",function (  $builder){
            return $builder->from("users")->select(["id"]);
        })->forceDelete();
})->describe('Display an inspiring quote');

Artisan::command('tests:register_have_pass', function () {
    $reg = new RegisterController();
    $users = User::whereNotNull('password')->whereNull('test_app_id')->get();
    foreach ($users as $user) {
        $randStr = substr(sha1(rand()), 0, 7);
        $id = $reg->registerUserToTests($user,$randStr);
        $user->test_app_id = $id;
        $user->save();
    }
})->describe('register to tests all users with password and no test_app_id');