<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 16 דצמ 18
 * Time: 15:36
 */


return [
	"contact" => env("RECIPIENT_CONTACT", "tithalal@yhb.org.il"),
	"askRabbi" => env("RECIPIENT_ASK_RABBI", "oren.matza@gmail.com"),
	"bookOrder" => env("RECIPIENT_BOOK_ORDER", "tithalal@yhb.org.il")

];