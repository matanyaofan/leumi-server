console.log(+process.env.PROD_ENV === 1? "production":"development");
module.exports = {
	"env": {
		"browser": true,
		"es6": true
	},
	extends: [
		// add more generic rulesets here, such as:
		// 'eslint:recommended',
		'plugin:vue/essential'
	],
	"parserOptions": {
		"sourceType": "module",
		"ecmaVersion": 8,
	},
	"rules": {
		// "indent": [
		//     "error",
		//     "tab"
		// ],
		"linebreak-style": +process.env.PROD_ENV === 1 ? 0 : [
			"error",
			"windows"
		],
		"quotes": [
			"error",
			"double"
		],
		"semi": [
			"error",
			"always"
		],
		"vue/name-property-casing": ["error", "kebab-case"],
		"vue/script-indent": ["error", "tab", {
			"baseIndent": 1,
			"switchCase": 0,
			"ignores": []
		}],
		"vue/html-indent": ["error", "tab", {
			"attribute": 1,
			"closeBracket": 0,
			"alignAttributesVertically": true,
			"ignores": []
		}]
	}
};