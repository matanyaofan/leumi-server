<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * App\SectionUser
 *
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SectionUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SectionUser newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\SectionUser onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SectionUser query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\SectionUser withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\SectionUser withoutTrashed()
 * @mixin \Eloquent
 */
class SectionUser extends Model
{
	use SoftDeletes;
	protected $fillable = ['user_id', "section_id", "book_id", "chapter_id"];

	public static function store( $data)
	{
		$data["user_id"] = Auth::id();
		$sectionUser = self::query()->where('user_id', $data["user_id"])->
		where("book_id", $data["book_id"])
			->where('chapter_id', $data["chapter_id"])
			->where('section_id', $data["section_id"])
			->first();
		if (isset($sectionUser->id))
			return $sectionUser;
		return SectionUser::create($data);
	}
}
