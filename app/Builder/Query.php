<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 08/05/18
 * Time: 08:37
 */

namespace App\Builder;

use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Query\Grammars\Grammar;
use Illuminate\Database\Query\Processors\Processor;
use Illuminate\Support\Str;
use function in_array;

class Query extends QueryBuilder
{

	/** @var Model $callerClass */
	private $callerClass;

	public function __construct(ConnectionInterface $connection, Grammar $grammar = null, Processor $processor = null)
	{
		parent::__construct($connection, $grammar, $processor);


	}


	public function joinQuery(Builder $query, $as, $first, $operator, $second, $type = "inner")
	{
		$this->join($this->selectSub($query, $as), $first, $operator, $second, $type);
		//$this->join(\DB::raw("(" . $query->toSql() . ") as $as"), $first, $operator, $second, $type);
		$this->setBindings($query->getBindings());
		return $this;
	}

	public function joinBelongsTo($className, $attributes = [])
	{
		$foreignKey = $this->getCallerClass()->getForeignKey();
		/** @var Model $joinModel */
		$joinModel = new $className;
		$joinName = $joinModel->getTable();
		if (!empty($attributes)) {
			/** @var Builder $query */
			$query = $joinModel->newQuery();
			if (!in_array($foreignKey, $attributes)) {
				$attributes[] = $foreignKey;
			}
			$query->select($attributes);
			$asTableName = $joinName . Str::random(6);
			$this->joinQuery($query, $asTableName, "$asTableName." . $this->getCallerClass()->getForeignKey(), "=", $joinModel->getKeyName(), "left");
		} else {
			$this->leftJoin($joinModel->getTable(), "$joinName." . $this->getCallerClass()->getForeignKey(), "=", $this->getCallerClass()->getTable() . "." . $this->getCallerClass()->getKeyName());
		}
		return $this;
	}


	/**
	 * @return Model
	 */
	public function getCallerClass()
	{
		return $this->callerClass;
	}

	/**
	 * @param Model $callerClass
	 * @return Query
	 */
	public function setCallerClass(Model $callerClass)
	{
		$this->callerClass = $callerClass;
		return $this;
	}

}