<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 08/05/18
 * Time: 08:38
 */

namespace App\Builder;


trait QueryUtils
{


	/**
	 * Get a new query builder instance for the connection.
	 *
	 * @return Query
	 */
	protected function newBaseQueryBuilder()
	{
		$connection = $this->getConnection();

		return (new Query(
			$connection, $connection->getQueryGrammar(), $connection->getPostProcessor()
		))->setCallerClass($this);
	}

}