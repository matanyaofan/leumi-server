<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Section
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Section newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Section newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Section query()
 * @mixin \Eloquent
 */
class Section extends Model
{
	CONST TFILAT_NASHIM = 3;
	const TFILAT_NASHIM_NAME="תפילת נשים";
	const TFILAT_NASHIM_CHAPTERS=[8,10,9,3,9,7,7,10,7,7,10,14,10,6,7,14,7,3,3,19,4,11,11,6];
	CONST SHABAT = 1;
	CONST SHABAT_A_NAME = "שבת א";
	CONST SHABAT_B_NAME = "שבת ב";
	CONST SHABAT_CHAPTERS = [16, 12, 5, 8, 15, 10, 8, 8, 12, 25, 18, 12, 16, 9, 14, 8, 18, 6, 11, 10, 15, 19, 15, 10, 9, 10, 17, 14, 8, 14];
	CONST TFILA = 2;
	const TFILA_NAME="תפילה";
	const TFILA_CHAPTERS=[10,10,11,8,11,9,3,8,6,7,12,10,6,6,12,7,21,10,9,11,8,9,12,7,9,3];
	CONST BRACHOT = 10;
	const BRACHOT_NAME="ברכות";
	const BRACHOT_CHAPTERS=[10,17,12,14,15,13,8,17,10,13,15,13,15,6,25,10,17,5];
	CONST MISHPAHA = 11;
	const MISHPAHA_NAME="משפחה ";
	const MISHPAHA_CHAPTERS=[30,18,12,20,24,12,22,13,10,14];

	protected $fillable = ["book_id", "chapter_num", "chapters_sections_length","book_name"];

}
