<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
	CONST STUDY_REMINDER = 0;
	CONST DAILY_REMINDER = 1;
	protected $fillable = ["user_id", "hour", "type"];

	public static function getCurrentUsers($type)
	{
		$query = self::query()
			->where("hour", Carbon::now("Asia/Jerusalem")->format('H:i'))
			->where("type", $type)
			->join("users", "users.id", "=", "notifications.user_id");

		$users = $query
			->whereNotNull("users.OS_id")
			->pluck("OS_id")
			->all();

		$notifications = $query
			->pluck("notifications.id")
			->all();
		return [
			'users' => $users,
			'notifications' => $notifications];

	}
}
