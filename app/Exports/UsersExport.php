<?php

namespace App\Exports;

use App\Repositories\Users\UserSearch;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use function implode;

class UsersExport implements FromCollection, WithHeadings, WithMapping
{
	/**
	 * @return \Illuminate\Support\Collection
	 */
	public function collection()
	{
		return (new UserSearch)->notAdmin()->getQuery()->get();
	}

	/**
	 * @return array
	 */
	public function headings(): array
	{
		return [
			"#",
			"שם",
			"אימייל",
			"פלאפון",
			"תעודת זהות",
			"כתובת",
			"מיקוד",
			"שירות לאומי",
			"ארגון",
			"תאריך הצטרפות"
		];
	}

	/**
	 * @param User $user
	 *
	 * @return array
	 */
	public function map($user): array
	{
		return [
			$user->id,
			$user->name,
			$user->email,
			$user->phone_number,
			$user->tz,
			implode(" ", [$user->city, $user->street, $user->home_number]),
			$user->ZIP_code,
			$user->leumi_place,
			$user->organization,
			Date::dateTimeToExcel($user->created_at),
		];
	}
}
