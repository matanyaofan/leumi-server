<?php
namespace App\Repositories\BookOrders;

use App\Book;
use Closeapp\Repository\Search;

class BookOrderSearch extends Search
{
    public function whereUserId($userId)
    {
        // $this->joinBelongsTo(Book::query(), ["name"]);
        $this->query->with('book');
        $this->query->where('user_id', $userId);
        return $this;
    }

}
