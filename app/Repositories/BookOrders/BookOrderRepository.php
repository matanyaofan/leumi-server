<?php
namespace App\Repositories\BookOrders;

use Closeapp\Repository\Repository;


class BookOrderRepository extends Repository
{

	public function find($id)
	{
		return (new BookOrderSearch())->find($id);
	}

	public function all($filters = [])
	{
		return (new BookOrderSearch())->setFilters($filters)->getQuery()->get();
	}
}
