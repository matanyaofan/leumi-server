<?php
/**
 * Created by PhpStorm.
 * User: Lior
 * Date: 05/12/2018
 * Time: 09:37
 */

namespace App\Repositories\Repository;


use App\Repositories\Search\SectionSearch;

class SectionRepository
{

	public function byBook($bookId)
	{
		$search = new SectionSearch(true);
		$search->whereBook($bookId);
		return $search->getQuery();
	}
}