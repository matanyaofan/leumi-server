<?php
namespace App\Repositories\Users;

use Closeapp\Repository\Repository;


class UserRepository extends Repository
{

	public function find($id)
	{
		return (new UserSearch())->find($id);
	}

	public function all($filters = [])
	{
		return (new UserSearch())->setFilters($filters)->notAdmin()->getQuery()->get();
	}
}
