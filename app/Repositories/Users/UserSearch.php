<?php
namespace App\Repositories\Users;


use App\User;
use Closeapp\Repository\Search;

class UserSearch extends Search
{


	public function notAdmin()
	{
		$this->query->where("role", User::STUDENT_ROLE);
		return $this;
	}
}
