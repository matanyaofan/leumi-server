<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 27/06/18
 * Time: 12:31
 */

namespace App\Repositories\Search;


use Closeapp\JoinQuery\Join;
use Illuminate\Database\Query\Builder;
use Request;
use function collect;

abstract class Search
{
	/**
	 * @var \App\Builder\Query|\Illuminate\Database\Eloquent\Builder
	 */
	protected $query;


	public function __construct($withRequestFilters = false)
	{
		$this->withRequestFilters($withRequestFilters);
	}

	/**
	 * @return \App\Builder\Query|\Illuminate\Database\Eloquent\Builder
	 */
	public function getQuery()
	{
		return $this->query;
	}

	public function whereId($id)
	{
		$this->query->where("id", $id);
		return $this;
	}



	/**
	 * @param array $columns
	 * @return \App\Builder\Query|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
	 */
	public function first($columns = ['*'])
	{
		return $this->query->first($columns);
	}

	/**
	 * @param array $columns
	 * @return \App\Builder\Query|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed|null
	 */
	public function find($columns = ['*'])
	{
		return $this->query->find($columns);
	}


	/**
	 * @param array $columns
	 * @return $this
	 */
	public function select($columns = ['*'])
	{
		$this->getQuery()->select($columns);
		return $this;
	}

	/**
	 * @param $className
	 * @param $attributes
	 * @param $type
	 * @return Search
	 * @throws \Exception
	 */
	public function joinBelongsTo($className, array $attributes = [], $type = "left")
	{
		Join::asBelongsTo($this->getQuery(), $className, $attributes, $type);
		return $this;
	}

	/**
	 * @param $className
	 * @param array $attributes
	 * @param string $type
	 * @return $this
	 * @throws \Exception
	 */
	public function joinHasOne($className, array $attributes = [], $type = "left")
	{
		Join::asHasOne($this->getQuery(), $className, $attributes, $type);
		return $this;
	}

	/**
	 * @param $query
	 * @param $as
	 * @param $first
	 * @param $operator
	 * @param $second
	 * @param string $type
	 * @return $this
	 */
	public function joinQuery($query, $as, $first, $operator, $second, $type = "inner")
	{
		Join::joinQuery($this->getQuery(), $query, $as, $first, $operator, $second, $type);
		return $this;
	}

	public function withRequestFilters($withRequestFilters = true)
	{
		if ($withRequestFilters) {
			$this->applyFilter(Request::all());
		}
	}

	private function applyFilter(array $filters)
	{
		$filtersCollect = collect($filters);
		if ($filtersCollect->has("orderBy")) {
			$reverse = $filtersCollect->get("reverse", 0) ? "desc" : "asc";
			$this->query->orderBy($filtersCollect->get("orderBy"), $reverse);
		}
	}


	public static function fromQuery(Builder $query)
	{
		$search = new static();
		$table = $search->query->getModel()->getTable();
		$query->from($table);
		$search->query = $query;
		return $search;
	}


}