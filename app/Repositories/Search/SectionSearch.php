<?php
/**
 * Created by PhpStorm.
 * User: Lior
 * Date: 05/12/2018
 * Time: 09:38
 */

namespace App\Repositories\Search;


use App\Section;

class SectionSearch extends Search
{

	/**
	 * MemberSearch constructor.
	 * @param bool $withRequestFilters
	 */
	public function __construct($withRequestFilters = false)
	{
		$this->query = Section::query();
		parent::__construct($withRequestFilters);
	}

	public function whereBook($bookId)
	{
		return $this->query->where("book_id", $bookId);
	}


}