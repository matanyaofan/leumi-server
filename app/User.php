<?php

namespace App;

use App\Mail\ResetPasswordNotification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Storage;

/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BookOrder[] $orders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Section[] $sections
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\User withoutTrashed()
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
	use Notifiable, SoftDeletes;
	const STUDENT_ROLE = 0;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'email', 'name', 'role', 'password', 'address','leumi_place', 'phone_number', 'organization', 'OS_id', 'daily_reminder', 'study_reminder', "image", 'current_book_number','tz', 'city', 'street', 'ZIP_code', 'home_number', 'login_service', 'test_app_id'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];


	/**
	 * @return string
	 */
	public function createToken()
	{
		return \Crypt::encrypt(["id" => $this->id]);
	}

	public static function getDailyNotification()
	{
		return User::query()
			->whereNotNull("OS_id")
			->where("daily_reminder", Carbon::now("Asia/Jerusalem")->format('H:i'))
			->pluck("OS_id")
			->all();
	}

	public static function getStudyNotification()
	{
		return User::query()
			->whereNotNull("OS_id")
			->where("study_reminder", Carbon::now("Asia/Jerusalem")->format('H:i'))
			->pluck("OS_id")
			->all();
	}

	public function sections()
	{
		return $this->belongsToMany('App\Section')->withTimestamps();
	}

	public function orders()
	{
		return $this->belongsToMany('App\BookOrder')->withTimestamps();
	}

	public function getImageFullPath($more = "")
	{
		if (isset($this->image)) {
			if (strpos($this->image, "https://") === false)
				return config("app.url") . Storage::url('images/' . $this->image) . "?update=" . $this->updated_at . $more;
			return $this->image;
		}
		return null;
	}

	public function sendPasswordResetNotification($token)
	{
		$this->notify(new ResetPasswordNotification($token));

	}

}
