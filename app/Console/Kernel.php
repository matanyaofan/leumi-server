<?php

namespace App\Console;

use App\HebDate\HebrewEvent;
use App\Http\Controllers\Api\NotificationController;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		//
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->call(function () {

			if (Carbon::today()->dayOfWeek != 6 && !self::isYomTov()) {
				NotificationController::dailyPush();
				NotificationController::repeatNotification();
			}
		})->everyMinute();
		$schedule->command('backup:clean')->daily()->at('01:00');
		$schedule->command('backup:run --only-db')->daily()->at('02:00');

	}

	protected static function isYomTov()
	{
		$events = HebrewEvent::getEventsToday();
		//$events = HebrewEvent::getEventsInDate(Carbon::createFromDate(2018,3,31));
		foreach ($events as $event) {
			/** @var HebrewEvent $event */
			if ($event->isYomTov())
				return true;
		}
		return false;
	}

	/**
	 * Register the commands for the application.
	 *
	 * @return void
	 */
	protected function commands()
	{
		$this->load(__DIR__ . '/Commands');

		require base_path('routes/console.php');
	}
}
