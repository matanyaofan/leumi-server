<?php
/**
 * Created by PhpStorm.
 * User: Lior
 * Date: 07/03/2018
 * Time: 09:45
 */

namespace App\HebDate;

use function json_decode;
use function json_encode;
use function strpos;
use Carbon\Carbon;


class HebrewEvent
{

	private $baseData;

	private function __construct($baseData)
	{
		$this->baseData = $baseData;
	}

	private static $events = null;


	public static function getEventsToday()
	{
		return self::getEventsInDate(Carbon::today());
	}

	public static function getEventsInDate(Carbon $theDay)
	{
		$times = self::checkForEvents();
		$events = [];
		foreach ($times as $time) {
			$date = Carbon::parse($time->date);
			if ($date->isSameDay($theDay)) {
				$events[] = new self($time);
			}
		}
		return $events;
	}

	public function getType()
	{
		return self::checkType($this->baseData->hebrew);
	}

	/**
	 * @return bool
	 */
	public function isYomTov()
	{
		return isset($this->baseData->yomtov) ? $this->baseData->yomtov : false;
	}

	public function __toString()
	{
		return $this->baseData;
		return json_encode($this->baseData);
	}

	private static function getTypes()
	{
		return [
			["label" => "ראש השנה", "code" => 11],
			["label" => "ראש השנה יום ב", "code" => 24],
			["label" => "צום גדליה", "code" => 18],
			["label" => "יום כפור", "code" => 12],
			["label" => "סוכות יום ז׳ (הושענא רבה)", "code" => 13],
			["label" => "חול המועד סוכות", "code" => 14],
			["label" => "שמיני עצרת", "code" => 15],
			["label" => "חנוכה", "code" => 9],
			["label" => "עשרה בטבת", "code" => 19],
			["label" => "תענית אסתר", "code" => 23],
			["label" => "פורים", "code" => 8],
			["label" => "פורים - מוקפות חומה", "code" => 10],
			["label" => "פסח", "code" => 16],
			["label" => "חול המועד פסח", "code" => 17],
			["label" => "פסח יום ו׳ (חול המועד)", "code" => 21],
			["label" => "יום העצמאות", "code" => 6],
			["label" => "שבועות", "code" => 22],
			["label" => 'צום תמוז', "code" => 20],
			["label" => "תשעה באב", "code" => 7],
		];
	}

	private static function checkType($name)
	{
		$types = self::getTypes();
		foreach ($types as $type) {
			if (!(strpos($name, $type["label"]) === false)) {
				return $type["code"];
			}
		}
		return false;
	}

	public function getHagName()
	{
		return $this->baseData->hebrew;
	}

	public function isErevHag()
	{
		return !(strpos($this->baseData->hebrew, "ערב") === false);
	}

	public static function getMonthEvent()
	{
		$times = self::checkForEvents();


		$timesArr = [];
		foreach ($times as $time) {
			$timesArr[] = new self($time);
		}
//		dd($timesArr);
		return $timesArr;
	}

	private static function checkForEvents()
	{
		if (!self::$events) {

			$curl = curl_init();
			$today = Carbon::now()->addMonth(0);
			curl_setopt_array($curl, array(
				CURLOPT_URL => "http://www.hebcal.com/hebcal/?v=1&cfg=json&year=$today->year&month=$today->month&maj=on&min=on&mf=on&mod=on",
//				CURLOPT_URL => "http://www.hebcal.com/hebcal/?v=1&cfg=json&year=$today->year&maj=on&min=on&mf=on&mod=on",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => ["Cache-Control: no-cache"],
			));

			$response = json_decode(curl_exec($curl));
			$err = curl_error($curl);
			curl_close($curl);

			if ($err) {

			} else {
//				if ($response)
//					self::$events = $response->items;
//				else
					self::$events = [];
			}
		}
		return self::$events;
	}
}
