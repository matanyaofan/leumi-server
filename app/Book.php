<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * App\Book
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book query()
 * @mixin \Eloquent
 */
class Book extends Model
{
	use Notifiable;
	CONST TFILAT_NASHIM = 3;
	CONST TFILAT_NASHIM_NAME = "תפילת נשים";
	CONST SHABAT = 1;
	CONST SHABAT_A_NAME = "שבת א";
	CONST SHABAT_NAME = "שבת";
	CONST SHABAT_B_NAME = "שבת ב";
	CONST SHABAT_CHAPTERS = [16, 12, 5, 8, 15, 10, 8, 8, 12, 25, 18, 12, 16, 9, 14, 8, 18, 6, 11, 10, 15, 19, 15, 10, 9, 10, 17, 14, 8, 14];
	CONST TFILA = 2;
	CONST TFILA_NAME = "תפילה";
	CONST BRACHOT = 10;
	CONST BRACHOT_NAME = "ברכות";
	CONST MISHPAHA = 11;
	CONST MISHPAHA_NAME = "משפחה ";

	protected $fillable = ['id', 'name'];
}
