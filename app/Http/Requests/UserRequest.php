<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [
			'is_password' => ['nullable'],
			'name' => ['required_without:is_password', 'string', 'max:255'],
			'role' => ['nullable', 'integer'],
			'password' => ['string', 'min:6'],
			'address' => ['nullable', 'string', 'max:255'],
			'tz' => ['nullable', 'string', 'max:255'],
			'token' => ['nullable', 'string'],
			'city' => ['nullable', 'string', 'max:255'],
			'street' => ['nullable', 'string', 'max:255'],
			'home_number' => ['nullable', 'string', 'max:255'],
			'ZIP_code' => ['nullable', 'string', 'max:255'],
			'leumi_place' => ['nullable', 'string', 'max:255'],
			'OS_id' => ['nullable', 'string', 'max:255'],
			'daily_reminder' => ['nullable'],
			'study_reminder' => ['nullable'],
			'phone_number' => ['nullable', 'string'],
			'organization' => ['nullable', 'string'],
			'login_service' => ['nullable', 'string']
		];
		if (Request::get("id"))
			$rules["email"] = ['required_without:is_password', 'string', 'email', 'max:255', Rule::unique('users')->ignore(Request::get("id"), 'id')];
		else if (Request::get("token"))
			$rules["email"] = ['required', 'string', 'email', 'max:255'];
		else if (Request::get("isNewVersion"))
			$rules["email"] = "required_without:is_password| email|string|unique:users";
		else $rules["email"] = "required_without:is_password|email|string";
		return $rules;
	}

	public function isUpdateBook()
	{
		return isset($this->bok_num);
	}
}
