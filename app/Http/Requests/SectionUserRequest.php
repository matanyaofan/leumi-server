<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SectionUserRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			"section_id" => "required_without:sections",
			"book_id" => "required_without:sections| integer",
			"chapter_id" => "required_without:sections| integer",
			"sections" => "required_without:chapter_id,book_id,section_id"
		];
	}

	function isManySections(){
		return isset($this->sections);
	}
}
