<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Book $book
	 * @return \Illuminate\Http\Response
	 */
	public function show(Book $book)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Book $book
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Book $book)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \App\Book $book
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Book $book)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Book $book
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Book $book)
	{
		//
	}

	public function initBooks()
	{
		Book::create([
			'id' => Book::BRACHOT,
			"name" => Book::BRACHOT_NAME]);
		Book::create([
			'id' => Book::TFILA,
			"name" => Book::TFILA_NAME]);
		Book::create([
			'id' => Book::TFILAT_NASHIM,
			"name" => Book::TFILAT_NASHIM_NAME]);
		Book::create([
			'id' => Book::MISHPAHA,
			"name" => Book::MISHPAHA_NAME]);
		Book::create([
			'id' => Book::SHABAT,
			"name" => Book::SHABAT_A_NAME]);
		Book::create([
			'id' => Book::SHABAT,
			"name" => Book::SHABAT_B_NAME]);
	}
}
