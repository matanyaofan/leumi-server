<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Auth\RegisterController;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use function str_random;

class UserController extends Controller
{
	public function login(Request $request)
	{
		$id = $request->id;
		$token = $request->token;

		if (!$token) {
			abort(401, "unauthorized");
		}
		/** @var array $decrypt */
		$decrypt = \Crypt::decrypt($token);
		if (!empty($decrypt["id"]) && $decrypt["id"] == $id) {
			/** @var User $user */
			if ($user = User::find($id)) {
				Auth::login($user);
				if (!isset($user->test_app_id)) {
					$user->test_app_id = isset($this->loadTestAppId($user)->_id) ? $this->loadTestAppId($user)->_id : null;
					$user->save();
				}
				$user->image = $user->getImageFullPath();
				$user->token = $user->createToken();
				return $user;
			}
			abort(401, "unauthorized");
		}
		abort(401, "unauthorized");
	}

	private function loadTestAppId(User $user)
	{
		$url = config("services.test_server") . 'api/user/email?email=' . $user->email;
		if ($this->get_http_response_code($url) == "200") {
			$response = file_get_contents($url);
			$json_clean = json_decode($response);
			return $json_clean;
		}

	}

	function get_http_response_code($url)
	{
		$headers = get_headers($url);
		return substr($headers[0], 9, 3);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param User $user
	 * @return response
	 */
	public function show(User $user)
	{
		/** @var User $user */
		$user = User::findOrFail($user->id);
		if (isset($user->image))
			$user->image = $user->getImageFullPath();
		return response()->json($user);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param UserRequest $request
	 * @param User $user
	 * @return Response
	 */
	public function update(UserRequest $request, User $user)
	{
		try {
			if ($request->isUpdateBook())
				$user->update(["current_book_num" => $request->book_num]);
			else {
				$user = $this->updateUser($request, $user);
			}

			return $this->show($user);
		} catch (\Exception $e) {
			return response()->json(["errors" => ['test' => ['פרטים עודכנו בהצלחה אך ההרשמה לאתר בחינות נכשלה. סיבה: ' . $e->getMessage() . '. ']]], 422);
		}
	}

	/**
	 *
	 * @param  $id
	 * @param mixed $photo
	 * @return string
	 */
	protected function getImageName($id, $photo)
	{
		return "user_" . $id . "." . $photo->extension();
	}

	protected function updateUser(UserRequest $request, User $user)
	{

		$data = $request->validated();
		$data["role"] = 0;
		if (isset($data["password"]))
			$data["password"] = encrypt($request->password);
		$user->update($data);
		if (!isset($user->test_app_id) && isset($request->password)) {
			$user->test_app_id = (new RegisterController())->registerUserToTests($user, $request->password);
			$user->save();
		}
		$user->image = $user->getImageFullPath();
		return $user;

	}

	public function updateImage(Request $request)
	{
		/** @var User $user */
		$user = \Auth::user();
		$image = $request->image;  // your base64 encoded
		$image = str_replace('data:image/png;base64,', '', $image);
		$image = str_replace(' ', '+', $image);
		$imageName = "user_" . $user->id . '.' . 'png';
		\Storage::disk("public")->put("images/" . $imageName, base64_decode($image));
		$user->image = $imageName;
		$user->save();
		return $user->getImageFullPath(str_random(5));
//\File::put(storage_path() . '/' . $imageName, base64_decode($image));
	}


}
