<?php

namespace App\Http\Controllers\Api;

use App\Book;
use App\BookOrder;
use App\Http\Controllers\Controller;
use App\Http\Requests\BookOrderRequest;
use App\Mail\BookOrder as BookOrderMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\BookOrders\BookOrderRepository; 

class BookOrderController extends Controller
{
		/**
	 * @var  BookOrderRepository
	 */
	protected $repository;

	public function __construct(BookOrderRepository $repository)
	{
		$this->repository = $repository;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return BookOrder[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
	 */
	public function index()
	{
		$id = Auth::user()->id;
		$orders = $this->repository->all(['user_id' => $id]);
		return $orders;
		// BookOrder::query()->where('user_id', $id)->get();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param BookOrderRequest $request
	 * @return void
	 */
	public function store(BookOrderRequest $request)
	{
		$data = $request->validated();
		$data['user_id'] = Auth::id();
		if ($data["address"] != 'replace') {
			$order = BookOrder::create($data);
			$book = Book::query()->where('id', $request->book_id)->first();
			$book_name = $book->name;
			\Mail::to(config("recipient.bookOrder"))->send(new BookOrderMail($order, $book_name, Auth::user()));
		}
		$user = User::find(Auth::id());
		$user->update([
			'current_book_number' => $request->book_id
		]);
		if (isset($user->image))
			$user->image = $user->image = $user->getImageFullPath();
		return $user;

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\BookOrder $book_order
	 * @return \Illuminate\Http\Response
	 */
	public function show(BookOrder $book_order)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\BookOrder $book_order
	 * @return \Illuminate\Http\Response
	 */
	public function edit(BookOrder $book_order)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \App\BookOrder $book_order
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, BookOrder $book_order)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\BookOrder $book_order
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(BookOrder $book_order)
	{
		//
	}
}
