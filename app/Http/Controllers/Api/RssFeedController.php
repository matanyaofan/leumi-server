<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Feeds;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;


class RssFeedController extends Controller
{
	public function index()
	{
		$feed = Feeds::make(' http://yom-yom.yhb.org.il/category/tithalal/feed/', 0, true);


		foreach ($feed->get_items() as $item) {
			$arr[] = ['content' => $item->data["child"]["http://purl.org/rss/1.0/modules/content/"]["encoded"][0]["data"],
				'date' => $item->data["child"][""]['pubDate'][0]['data']
			];
		};
		return $arr;
	}

	public function show()
	{
		$feed = Feeds::make(' http://yom-yom.yhb.org.il/category/tithalal/feed/', 0, true);

		return $feed->get_items()[0]->data["child"]["http://purl.org/rss/1.0/modules/content/"]["encoded"][0]["data"];

	}
}
