<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\NotificationRequest;
use App\Notification;
use App\Notification\MyOneSignalClient;
use App\User;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param NotificationRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(NotificationRequest $request)
	{
		$data = $request->validated();
		$data["user_id"] = Auth::id();
		return Notification::create($data);
	}


	public static function dailyPush()
	{
		self::push(
			User::getDailyNotification(),
			"פותחים את היום בהלכה", null, "30 שניות הלכה יומית"
		);
		self::push(
			User::getStudyNotification(),
			"זה הזמן ללימוד יומי!"
		);
	}

	public static function push($users, $text, $url = null, $title = null)
	{
		$oneSignal = new MyOneSignalClient();
		if (count($users)) {
			return $oneSignal->sendNotificationToUsers($text, $users, $url, $title);
		}
		return true;
	}

	public static function repeatNotification()
	{
		$notifications = Notification::getCurrentUsers(Notification::STUDY_REMINDER);
		self::push($notifications["users"], "זה הזמן ללימוד יומי!");
		Notification::destroy($notifications["notifications"]);
	}
}
