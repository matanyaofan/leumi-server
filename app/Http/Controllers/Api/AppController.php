<?php
/**
 * Created by PhpStorm.
 * User: Lior
 * Date: 27/11/2018
 * Time: 10:13
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Mail\AskRabbi;
use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use function config;

class AppController extends Controller
{
	/**
	 * @param Request $request
	 * @return array
	 */
	public function sendMail(Request $request)
	{
		$title = $request->input("title");
		$user = Auth::user();
		if ($title === "contact") {
			Mail::to(config("recipient.contact"))->send(new Contact($request, $user));
		}
		if ($title === "askRabbi") {
			Mail::to(config("recipient.askRabbi"))->send(new AskRabbi($request, $user));
		}
		return response()->json(true, 200);
	}

}