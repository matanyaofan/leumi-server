<?php

namespace App\Http\Controllers\Api;

use App\Book;
use App\Http\Controllers\Controller;
use App\Repositories\Repository\SectionRepository;
use App\Section;
use Illuminate\Http\Request;

class SectionController extends Controller
{

	/* @var SectionRepository $repository */
	protected $repository;

	/**
	 * ClassroomController constructor.
	 * @param SectionRepository $sectionRepository
	 */
	public function __construct(SectionRepository $sectionRepository)
	{
		$this->repository = new $sectionRepository;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 * @return \App\Builder\Query[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
	 */
	public function index(Request $request)
	{
		if ($this->isByBook($request))
			return $this->repository->byBook($request->book_id)->orderBy('chapter_num', 'desc')->get();

		return Section::all();

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\section $section
	 * @return \Illuminate\Http\Response
	 */
	public function show(section $section)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\section $section
	 * @return \Illuminate\Http\Response
	 */
	public function edit(section $section)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \App\section $section
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, section $section)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\section $section
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(section $section)
	{
		//
	}

	public function initSections()
	{
		if (count(Book::all()) == 0) {
			Book::create([
				'id' => Book::BRACHOT,
				"name" => Book::BRACHOT_NAME]);
			Book::create([
				'id' => Book::TFILA,
				"name" => Book::TFILA_NAME]);
			Book::create([
				'id' => Book::TFILAT_NASHIM,
				"name" => Book::TFILAT_NASHIM_NAME]);
			Book::create([
				'id' => Book::MISHPAHA,
				"name" => Book::MISHPAHA_NAME]);
			Book::create([
				'id' => Book::SHABAT,
				"name" => Book::SHABAT_A_NAME]);
			Book::create([
				'id' => Book::SHABAT,
				"name" => Book::SHABAT_B_NAME]);
		}
		if (count(Section::all()) == 0) {
			for ($i = 0; $i < count(Section::BRACHOT_CHAPTERS); $i++) {
				Section::create([
					'book_id' => Section::BRACHOT,
					"chapter_num" => $i + 1,
					"book_name" => Section::BRACHOT_NAME,
					"chapters_sections_length" => Section::BRACHOT_CHAPTERS[$i]]);
			}
			for ($i = 0; $i < count(Section::MISHPAHA_CHAPTERS); $i++) {
				Section::create([
					'book_id' => Section::MISHPAHA,
					"chapter_num" => $i + 1,
					"book_name" => Section::MISHPAHA_NAME,
					"chapters_sections_length" => Section::MISHPAHA_CHAPTERS[$i]]);
			}
			for ($i = 0; $i < count(Section::TFILA_CHAPTERS); $i++) {
				Section::create([
					'book_id' => Section::TFILA,
					"chapter_num" => $i + 1,
					"chapters_sections_length" => Section::TFILA_CHAPTERS[$i],
					"book_name" => Section::TFILA_NAME]);
			}
			for ($i = 0; $i < count(Section::TFILAT_NASHIM_CHAPTERS); $i++) {
				Section::create([
					'book_id' => Section::TFILAT_NASHIM,
					"book_name" => Section::TFILAT_NASHIM_NAME,
					"chapter_num" => $i + 1,
					"chapters_sections_length" => Section::TFILAT_NASHIM_CHAPTERS[$i]]);
			}
			for ($i = 0; $i < count(Section::SHABAT_CHAPTERS); $i++) {
				Section::create([
					"book_name" => $i < 15 ? Section::SHABAT_A_NAME : Section::SHABAT_B_NAME,
					'book_id' => Section::SHABAT,
					"chapter_num" => $i + 1,
					"chapters_sections_length" => Section::SHABAT_CHAPTERS[$i]]);
			}
			return "success";
		}

		return "already exist";
	}

	function isByBook($request)
	{
		return (isset($request->book_id));
	}

	public function updateSections()
	{

		for ($i = 0; $i < count(Section::BRACHOT_CHAPTERS); $i++) {
			$section = Section::query()->where('book_id', Section::BRACHOT)->where("chapter_num", $i + 1)->first();
			$section->update([
				"chapters_sections_length" => Section::BRACHOT_CHAPTERS[$i]]);
		}
		for ($i = 0; $i < count(Section::MISHPAHA_CHAPTERS); $i++) {
			$section = Section::query()->where('book_id', Section::MISHPAHA)->where("chapter_num", $i + 1)->first();
			$section->update([
				"chapters_sections_length" => Section::MISHPAHA_CHAPTERS[$i]]);
		}

		for ($i = 0; $i < count(Section::TFILA_CHAPTERS); $i++) {
			$section = Section::query()->where('book_id', Section::TFILA)->where("chapter_num", $i + 1)->first();
			$section->update([
				"chapters_sections_length" => Section::TFILA_CHAPTERS[$i]]);
		}

		for ($i = 0; $i < count(Section::TFILAT_NASHIM_CHAPTERS); $i++) {
			$section = Section::query()->where('book_id', Section::TFILAT_NASHIM)->where("chapter_num", $i + 1)->first();
			$section->update([
				"chapters_sections_length" => Section::TFILAT_NASHIM_CHAPTERS[$i]]);

		}
		for ($i = 0; $i < count(Section::SHABAT_CHAPTERS); $i++) {
			$section = Section::query()->where('book_id', Section::SHABAT)->where("chapter_num", $i + 1)->first();
			$section->update([
				"chapters_sections_length" => Section::SHABAT_CHAPTERS[$i]]);
		}
		return "success";

	}


}
