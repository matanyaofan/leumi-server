<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SectionUserRequest;
use App\Section;
use App\SectionUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class SectionUserController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
	 */
	public function index(Request $request)
	{
		if (isset($request->book_id))
			return SectionUser::query()->where("user_id", Auth::id())->where("book_id", $request->book_id)->get();
		return SectionUser::query()->where("user_id", Auth::id())->get();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param SectionUserRequest $request
	 * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
	 */
	public function store(SectionUserRequest $request)
	{

		$data = $request->validated();
		if ($request->isManySections()) {
			$sections = [];
			foreach ($request->sections as $section) {

				$sectionUser = SectionUser::store($section);
				array_push($sections, $sectionUser);
			}
			return $sections;
		} else
			$sectionUser = SectionUser::store($data);

		return $sectionUser;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\SectionUser $sectionUser
	 * @return \Illuminate\Http\Response
	 */
	public function show(SectionUser $sectionUser)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\SectionUser $sectionUser
	 * @return SectionUser
	 * @throws \Exception
	 */
	public function destroy($id)
	{
		$sectionUser = SectionUser::find($id);
		$sectionUser->delete();
		return $sectionUser;
	}

	public function deleteSectionFromUser(SectionUserRequest $request)
	{
		$request->validated();
		$sectionUser = SectionUser::query()
			->where("section_id", $request->section_id)
			->where("book_id", $request->book_id)
			->where("chapter_id", $request->chapter_id)
			->where("user_id", Auth::id())
			->delete();
		return $sectionUser;
	}

	public function deleteManySectionsFromUser(Request $request)
	{
		SectionUser::query()
			->where("chapter_id", $request->chapter_id)
			->where("book_id", $request->book_id)
			->where("user_id", Auth::id())
			->delete();
		return response()->json(["success" => true], 200);
	}

	public function deleteMistakeSections()
	{
		SectionUser::query()->where('book_id', Section::BRACHOT)->where('chapter_id', 14)->where('section_id', '>', 6)->delete();
		SectionUser::query()->where('book_id', Section::BRACHOT)->where('chapter_id', 17)->where('section_id', '>', 17)->delete();
		SectionUser::query()->where('book_id', Section::BRACHOT)->where('chapter_id', 18)->where('section_id', '>', 5)->delete();
		SectionUser::query()->where('book_id', Section::MISHPAHA)->where('chapter_id', 9)->where('section_id', '>', 10)->delete();
		SectionUser::query()->where('book_id', Section::MISHPAHA)->where('chapter_id', 7)->where('section_id', '>', 22)->delete();
	}
}
