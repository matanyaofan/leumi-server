<?php

namespace App\Http\Controllers\Api;

use App\Book;
use App\Http\Controllers\Controller;
use App\Mail\BookOrder;
use Illuminate\Http\Request;

class BookController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return Book[]|\Illuminate\Database\Eloquent\Collection
	 */
	public function index()
	{
		return Book::all();
	}

	public function show($bookNumber)
	{
		return Book::find($bookNumber);
	}


	public function initBooks()
	{
		Book::whereNotNull('id')->delete();
		Book::create([
			'id' => Book::BRACHOT,
			"name" => Book::BRACHOT_NAME]);
		Book::create([
			'id' => Book::TFILA,
			"name" => Book::TFILA_NAME]);
		Book::create([
			'id' => Book::TFILAT_NASHIM,
			"name" => Book::TFILAT_NASHIM_NAME]);
		Book::create([
			'id' => Book::MISHPAHA,
			"name" => Book::MISHPAHA_NAME]);
		Book::create([
			'id' => Book::SHABAT,
			"name" => Book::SHABAT_NAME]);

	}
}
