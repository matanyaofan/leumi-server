<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
//		$this->middleware('guest')->except(['logout', "auth"]);
	}


	public
	function auth()
	{
		/** @var User $user */
		$user = Auth::user();
		if (!isset($user->test_app_id)) {
			$user->test_app_id = isset($this->loadTestAppId($user)->_id) ? $this->loadTestAppId($user)->_id : null;
			$user->save();
		}
		$user->image = $user->getImageFullPath();
		$user->token = $user->createToken();

		return $user;
	}

	private function loadTestAppId(User $user)
	{
		$url = config("services.test_server") . 'api/user/email?email=' . $user->email;
		if ($this->get_http_response_code($url) == "200") {
			$response = file_get_contents($url);
			$json_clean = json_decode($response);
			return $json_clean;
		}

	}

	function get_http_response_code($url)
	{
		$headers = get_headers($url);
		return substr($headers[0], 9, 3);
	}

	protected
	function validateLogin(Request $request)
	{
		$request->validate([
			$this->username() => 'required|string',
			'password' => 'nullable|string',
			'OS_id' => 'nullable|string'
		]);
	}

	protected
	function attemptLogin(Request $request)
	{
		if (isset($request->email) && isset($request->password))
			return $this->guard()->attempt(
				$this->credentials($request), $request->filled('remember')
			);
		else if (isset($request->email)) {
			$user = $this->isExistMail($request->email);
			if (!isset($user))
				return false;
			else if (!isset($user->password) && !isset($user->login_service))
				return true;
		}

		return false;
	}

	public function authenticated(Request $request, $user)
	{
		if (!isset($user)) {
			$user = $this->isExistMail($request->email);
			Auth::login($user);
		}
		if (!isset($user->test_app_id)) {
			$testData = $this->loadTestAppId($user);
			if(isset($testData)) {
				$user->test_app_id = $testData->_id;
				$user->save();
			} else {
				if(isset($request->password)){
					$user->test_app_id = $this->registerUserToTests($user, $request->password);
					$user->save();
				}
			}
		}
		if(isset($request->OS_id) && $user->OS_id != $request->OS_id){
			$user->OS_id = $request->OS_id;
			$user->save();
		}
		if ($user->password == null && !$user->login_service)
			$user->needToChangePassword = true;
		$user->image = $user->getImageFullPath();
		$user->token = $user->createToken();
		return $user;
	}

	protected
	function isExistMail($email)
	{
		return User::query()->where("email", $email)->first();
	}

	/**
	 * Log the user out of the application.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function logout(Request $request)
	{
		$this->guard()->logout();

		$request->session()->invalidate();

		return $this->loggedOut($request);
	}

	public function registerUserToTests($user, $password)
	{
		$client = new Client();

// Create a POST request
		try {
			$response = $client->request(
				'POST',
				config("services.test_server") . 'api/user-app/register',
				['headers' => [
					'Content-Type' => 'application/json;charset=UTF-8',
					'Accept' => 'application/json, text/plain, */*',
				],
					'json' => [
						'email' => $user->email,
						'password' => $password,
						'name' => $user->name,
						'phone' => $user->phone_number,
						'type' => $user->login_service
					]]

			);
		} catch (\Exception $e) {
			if ($e->getCode() == '422') {
				if (str_contains($e->getMessage(), 'email already exist'))
					throw new \Exception("כתובת מייל תפוסה באתר המבחנים", 422);
				else if (str_contains($e->getMessage(), 'last name is missing'))
					throw new \Exception("חסר שם משפחה", 422);
				else if (str_contains($e->getMessage(), 'phone already exist'))
					throw new \Exception('מספר טלפון כבר תפוס', 422);
			} else throw new \Exception("שגיאת תקשורת, נסי שוב מאוחר יותר", 500);
//
		}


// Parse the response object, e.g. read the headers, body, etc.
		$headers = $response->getHeaders();
		$body = $response->getBody()->getContents();

// Output headers and body for debugging purposes

		$body = json_decode($body);
		return $body->_id;
	}


}
