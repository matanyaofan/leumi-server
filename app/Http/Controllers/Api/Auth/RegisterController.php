<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Requests\UserRequest;
use App\User;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{

	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/home';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
//		$this->middleware('guest');
	}

	public function register(UserRequest $request)
	{

		$data = $request->validated();
		$data['daily_reminder'] = '07:30:00';
		try {
			if (!isset($request->isNewVersion)) {
				return $this->oldVersionRegister($data);
			}
			event(new Registered($user = $this->create($data)));
			$this->guard()->login($user);
			return $user;
		} catch (\Exception $e) {
			return response()->json(["errors" => ['general' => [$e->getMessage()]]], 422);
		}

	}


	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array $data
	 * @return \App\User
	 * @throws \Exception
	 */
	protected function create(array $data)
	{
		$user = $this->registerNewUser($data);
		$user->image = $user->getImageFullPath();
		$user->token = $user->createToken();
		return $user;
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array $data
	 * @return \App\User
	 */
	protected function oldVersionRegister(array $data)
	{
		$user = $this->isExistMail($data["email"]);
		if (!isset($user->id))
			$user = $this->RegisterNewUserOldVersion($data);
		Auth::guard()->login($user);
		$user->image = $user->getImageFullPath();
		$user->token = $user->createToken();
		return $user;
	}

	protected function isExistMail($email)
	{
		return User::query()->where("email", $email)->first();
	}

	protected function RegisterNewUserOldVersion(array $data)
	{
		if (!isset($data['role']))
			$data["role"] = 0;
		$user = User::create($data);
		if (isset($data["image"])) {
			$this->storeImage($data["image"], $user);
		}
		return $user;
	}

	protected function storeImage($image, $user)
	{
		$imageName = $this->getImageName($user->id, $image);
		$image->storeAs('images', $imageName, "public");
		$user->image = $imageName;
		$user->save();
	}


	protected function registerNewUser(array $data)
	{
		try {
			if (!isset($data['role']))
				$data["role"] = 0;
			if (isset($data['password'])) {
				$password = $data['password'];
				$data['password'] = bcrypt($data['password']);
			} else {
				$data['login_service'] = 'google';
			}
			$user = User::create($data);
			if ($user->login_service == 'google')
				$password = $data['token'];
			$user->test_app_id = $this->registerUserToTests($user, $password);
			$user->save();
			if (isset($data["image"]) && strpos($data["image"], "https://") === false) {
				$this->storeImage($data["image"], $user);
			}
			return $user;
		} catch (\Exception $e) {
			$user->forceDelete();
			throw new \Exception($e->getMessage(), $e->getCode());
		}
	}


	/**
	 *
	 * @param  $id
	 * @param mixed $photo
	 * @return string
	 */
	protected function getImageName($id, $photo)
	{
		return "user_" . $id . "." . $photo->extension();
	}


	public function setPassword(UserRequest $request)
	{
		try {
			$request->validated();
			$user = Auth::user();
			$user->test_app_id = $this->registerUserToTests($user, $request->password);
			$user->update(['password' => bcrypt($request->password)]);
			$user->save();
			$user->image = $user->getImageFullPath();
			$user->token = $user->createToken();

			return $user;
		} catch (\Exception $e) {
			return response()->json(["errors" => ['test' => ['סיסמא עודכנה בהצלחה אך ההרשמה לאתר בחינות נכשלה. סיבה: ' . $e->getMessage() . '. ']]], $e->getCode());
		}
	}

	public function registerUserToTests($user, $password)
	{
		$client = new Client();

// Create a POST request
		try {
			$response = $client->request(
				'POST',
				config("services.test_server") . 'api/user-app/register',
				['headers' => [
					'Content-Type' => 'application/json;charset=UTF-8',
					'Accept' => 'application/json, text/plain, */*',
				],
					'json' => [
						'email' => $user->email,
						'password' => $password,
						'name' => $user->name,
						'phone' => $user->phone_number,
						'type' => $user->login_service
					]]

			);
		} catch (\Exception $e) {
			if ($e->getCode() == '422') {
				if (str_contains($e->getMessage(), 'email already exist'))
					throw new \Exception("כתובת מייל תפוסה באתר המבחנים", 422);
				else if (str_contains($e->getMessage(), 'last name is missing'))
					throw new \Exception("חסר שם משפחה", 422);
				else if (str_contains($e->getMessage(), 'phone already exist'))
					throw new \Exception('מספר טלפון כבר תפוס', 422);
			} else throw new \Exception("שגיאת תקשורת, נסי שוב מאוחר יותר", 500);
//
		}


// Parse the response object, e.g. read the headers, body, etc.
		$headers = $response->getHeaders();
		$body = $response->getBody()->getContents();

// Output headers and body for debugging purposes

		$body = json_decode($body);
		return $body->_id;
	}
}
