<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use Google_Client;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class GoogleLoginController extends Controller
{
	const CLIENT_ID = '218025113419-ack51osoagqdnq3cbakupr618ghcikl1.apps.googleusercontent.com';
	use AuthenticatesUsers;

	public function googleLogin(UserRequest $request)
	{
		try {

			if ($this->verifyLogin($request->token)) {
				if ($this->isRegisteredBy($request->email, 'google')|| $this->isRegisteredBy($request->email, null)){
				if(isset($request->OS_id)){
					$this->updateOsId($request->email, $request->OS_id);
				}	
				return $this->authenticated($request, $this->getUser($request->email));
			}
				else return $this->registerUser($request);
			} else return $this->sendFailedLoginResponse($request);
		} catch
		(ValidationException $e) {
			 return response()->json(["errors" => ['general' => [$e->getMessage()]]], 422);
		} catch
		(\Exception $e) {
			return response()->json(["errors" => ['general' => [$e->getMessage()]]], 422);
		}


	}

	protected function verifyLogin($token)
	{
		$client = new Google_Client(['client_id' => self::CLIENT_ID]);
		$payload = $client->verifyIdToken($token);
		return !!($payload);
	}

	protected function getUser($email)
	{
		return User::query()->where("email", $email)->first();
	}

	protected function isRegisteredBy($email, $service)
	{
		return !!(User::query()->where("email", $email)->where('login_service', $service)->first());
	}

	protected function updateOsId($email, $osId)
	{
		User::query()->where("email", $email)->update(['OS_id' => $osId]);
	}

	/**
	 * @param $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 * @throws \Exception
	 */
	protected function registerUser($request)
	{
		$request["login_service"] = 'google';
		return (new RegisterController())->register($request);
	}

	public function authenticated(Request $request, $user)
	{
		if (!isset($user->test_app_id)) {
			$user->test_app_id = isset($this->loadTestAppId($user)->_id) ? $this->loadTestAppId($user)->_id : null;
			$user->save();
		}
		$user->image = $user->getImageFullPath();
		$user->token = $user->createToken();
		return $user;
	}

	private function loadTestAppId(User $user)
	{
		$url = config("services.test_server") . 'api/user/email?email=' . $user->email;
		if ($this->get_http_response_code($url) == "200") {
			$response = file_get_contents($url);
			$json_clean = json_decode($response);
			return $json_clean;
		}

	}

	function get_http_response_code($url)
	{
		$headers = get_headers($url);
		return substr($headers[0], 9, 3);
	}



}
