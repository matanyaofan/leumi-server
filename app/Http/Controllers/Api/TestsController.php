<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \Firebase\JWT\JWT;
use App\Http\Controllers\Api\Auth\RegisterController;
use App\User;


class TestsController extends Controller
{


	public function getBestTable()
	{
		try {
			$client = new Client();

			$response = $client->request(
				'POST',
				config("services.test_server") . 'api/user-app/highScore'
			);
			return $response;
		} catch (\Exception $e) {
			return response()->json("שגיאת תקשורת, נסי שוב מאוחר יותר", 500);
		}

	}

	public function index()
	{
		$user = Auth::user();
		if(isset($user->test_app_id)){
			$id = $user->test_app_id;
		} else {
			$id = $this->loadTestAppId($user);
			if(isset($id)){
				$id = $id->_id;
				$user->test_app_id = $id;
				$user->save();
			} else {
				return response('No test ID', 401)
				->header('Content-Type', 'text/plain');

			}
		}

		if(!$id) {
			return response('No test ID', 401)
			->header('Content-Type', 'text/plain');
		}

		$client = new Client();

		try {
			return $client->request(
				'POST',
				config("services.test_server") . 'api/user-app/test',
				['headers' => [
					'Content-Type' => 'application/json;charset=UTF-8',
					'Accept' => 'application/json, text/plain, */*',
				],
					'json' => [
						'_id' => $id
					]]

			);
		} catch (\Exception $e) {
			return response()->json("שגיאת תקשורת, נסי שוב מאוחר יותר", 500);
		}

	}


	public function show()
	{
		$user = Auth::user();
		if(isset($user->test_app_id)){
			$id = $user->test_app_id;
		} else {
			$id = $this->loadTestAppId($user);
			if(isset($id)){
				$id = $id->_id;
				$user->test_app_id = $id;
				$user->save();
			} else {
				return response()->json("no test id", 500);				
			}
		}
		$date = Carbon::today();
//		$month = $date->month < 10 ? '0' . $date->month : $date->month;
		$key = $date->day . '_' . $date->month . '_' . $date->year . '';

		$token = array(
			"id" => $id,
		);
		$jwt = JWT::encode($token, $key);
		return $jwt;


	}

	public function isPassedTheTest(Request $request)
	{
		$id = Auth::user()->test_app_id;
		$client = new Client();

		try {
			return $client->request(
				'POST',
				config("services.test_server") . 'api/user-app/bookTestSuccessfully',
				['headers' => [
					'Content-Type' => 'application/json;charset=UTF-8',
					'Accept' => 'application/json, text/plain, */*',
				],
					'json' => [
						'_id' => $id,
						'bookName' => $request->bookName
					]]

			);
		} catch (\Exception $e) {
			return response()->json("שגיאת תקשורת, נסי שוב מאוחר יותר", 500);
		}

	}

	private function loadTestAppId(User $user)
	{
		$url = config("services.test_server") . 'api/user/email?email=' . $user->email;
		if ($this->get_http_response_code($url) == "200") {
			$response = file_get_contents($url);
			$json_clean = json_decode($response);
			return $json_clean;
		}

	}

	private function get_http_response_code($url)
	{
		$headers = get_headers($url);
		return substr($headers[0], 9, 3);
	}
}