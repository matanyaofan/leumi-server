<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/home';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
//		$this->middleware('guest');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => ['required', 'string', 'max:255'],
			'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
			'role' => ['nullable', 'integer'],
			'password' => ['nullable', 'string', 'min:6'],
			'address' => ['nullable', 'string', 'max:255'],
			'leumi_place' => ['nullable', 'string', 'max:255'],
			'OS_id' => ['nullable', 'string', 'max:255'],
			'daily_reminder' => ['nullable', 'time'],
			'study_reminder' => ['nullable', 'time'],
			'image' => ['nullable', 'image'],
			'phone_number' => ['nullable', 'string'],
			'organization' => ['nullable', 'string']
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array $data
	 * @return \App\User
	 */
	protected function create(array $data)
	{
		if (!isset($data['role']))
			$data["role"] = 0;
		$user = User::create($data);
		if (isset($data["image"])) {
			$imageName = $this->getImageName($user->id, $data["photo"]);
			$data["image"]->storeAs('images', $imageName, "public");
			$user->image = $imageName;
			$user->save();
		}
		Auth::guard()->login($user);
		$user->token = $user->createToken();
		return $user;
	}

	/**
	 *
	 * @param  $id
	 * @param mixed $photo
	 * @return string
	 */
	protected function getImageName($id, $photo)
	{
		return "user_" . $id . "." . $photo->extension();
	}

	/**
	 * The user has been registered.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  mixed $user
	 * @return mixed
	 */
	protected function registered(Request $request, $user)
	{
		return $user;
	}
}
