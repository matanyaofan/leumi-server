<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Notification\MessageNotification;
use function request;


class NotificationController extends Controller
{

	public function send()
	{
		//return [23,23];
		MessageNotification::send(request()->message, request()->link);
		return [];
	}
}
