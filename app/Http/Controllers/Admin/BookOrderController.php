<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Http\Requests\BookOrderRequest;
use App\Mail\TextMail;
use App\Repositories\BookOrders\BookOrderRepository;
use App\BookOrder;
use App\User;
use Illuminate\Database\Query\Builder;


class BookOrderController extends Controller
{

	/**
	* @var  BookOrderRepository
	*/
	protected $repository;

	public function __construct(BookOrderRepository $repository)
	{

		$this->repository = $repository;
	}

	public function index()
	{

		$bookorders = BookOrder::query()
            //TODO fix the database!
            ->whereIn("user_id",function (Builder $builder){
                return $builder->from("users")
                    ->whereNull("deleted_at")
                    ->select(["id"]);
            })
            ->with('user')->with('book')->get();

		return response()->json($bookorders);
	}

	public function store(BookOrderRequest $request)
	{
		$bookorder = BookOrder::create($request->validated());

		return response()->json($bookorder);
	}

	public function show($id)
	{
		$bookorder = BookOrder::findOrFail($id);
		$bookorder->load('user');

		return response()->json($bookorder);
	}

	public function update(BookOrderRequest $request, $id)
	{
		$bookorder = BookOrder::findOrFail($id);
		$data = $request->validated();

        $bookorder->book_send_date = date('Y-m-d H:i:s');
        $bookorder->update($data);
        $user = $bookorder->user;
        \Mail::to($user->email)->send(new TextMail('שלום רב','שמחים לעדכן כי הספר שהזמנת הועבר לשליחה ובדרכו אליך'));
		return response()->json($bookorder, 200);
	}

	public function destroy($id)
	{
		$bookorder = BookOrder::findOrFail($id);
		$bookorder->delete();


		return response()->json($bookorder,200);
	}
}
