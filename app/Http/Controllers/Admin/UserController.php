<?php

namespace App\Http\Controllers\Admin;
use App\Exports\UsersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Repositories\Users\UserRepository;
use App\User;
use Maatwebsite\Excel\Facades\Excel;


class UserController extends Controller
{

	/**
	* @var  UserRepository
	*/
	protected $repository;

	public function __construct(UserRepository $repository)
	{

		$this->repository = $repository;
	}

	public function index()
	{
		$users = $this->repository->all();

		return response()->json($users);
	}


	public function export()
	{
		return Excel::download(new UsersExport, 'users.xlsx');
	}


	public function store(UserRequest $request)
	{
		$user = User::create($request->validated());

		return response()->json($user);
	}

	public function show($id)
	{
		$user = User::findOrFail($id);

		return response()->json($user);
	}

	public function update(UserRequest $request, $id)
	{
		$user = User::findOrFail($id);
		$user->update($request->all());

		return response()->json($user, 200);
	}

	public function destroy($id)
	{
		$user = User::findOrFail($id);
		$user->delete();


		return response()->json($user,200);
	}
}
