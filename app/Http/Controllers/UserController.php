<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use function abort;
use function view;

class UserController extends Controller
{

	public function index(Request $request)
	{
		if ($request->token == "ppaesolc") {
			$users = User::get();
			return view("users")->with(["users" => $users]);
		}
		abort(401);
	}

}
