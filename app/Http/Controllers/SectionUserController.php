<?php

namespace App\Http\Controllers;

use App\SectionUser;
use Illuminate\Http\Request;

class SectionUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SectionUser  $sectionUser
     * @return \Illuminate\Http\Response
     */
    public function show(SectionUser $sectionUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SectionUser  $sectionUser
     * @return \Illuminate\Http\Response
     */
    public function edit(SectionUser $sectionUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SectionUser  $sectionUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SectionUser $sectionUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SectionUser  $sectionUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(SectionUser $sectionUser)
    {
        //
    }
}
