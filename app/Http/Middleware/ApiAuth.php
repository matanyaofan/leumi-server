<?php

namespace App\Http\Middleware;

use App\User;
use Crypt;
use Illuminate\Support\Facades\Auth;
use Closure;
use function abort;

class ApiAuth
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
//		$value = $request->header('Authorization');
		$id = $request->server("PHP_AUTH_USER");
		$token = $request->server("PHP_AUTH_PW");
//		abort(200, $token);

		if (!$token) {
			abort(401, "unauthorized");
		}
		/** @var array $decrypt */
		$decrypt = \Crypt::decrypt($token);
//		dd($decrypt);
		if (!empty($decrypt["id"]) && $decrypt["id"] == $id) {
			if ($user = User::find($id)) {
				Auth::login($user);
				return $next($request);
			}
			abort(401, "unauthorized");
		}
		abort(401, "unauthorized");

	}
}
