<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 28 ינו 19
 * Time: 16:53
 */

namespace App\Notification;


use function config;
use function dd;

class MessageNotification
{
	public function __construct()
	{

	}

	public static function send($message, $url = null)
	{
		$oneSignal = new MyOneSignalClient();
		$debug = config("app.debug");
		if ($debug) {
			return $oneSignal->sendNotificationToUsers(
				$message, [
				"90afc778-2dd3-41d2-82c7-c7f8fcf158a0",
				"9a542122-c4e2-4140-b4b2-1c0e79321792",
				"064b0690-56c7-4d3f-9d3b-9151b7cf7641",
//			"63d371d0-fde8-4fdc-adb6-cacc0fc8ea9d"
			], $url);
		}
		$oneSignal->sendNotificationToAll($message, $url);

	}

}
