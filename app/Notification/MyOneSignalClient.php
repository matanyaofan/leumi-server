<?php
/**
 * Created by PhpStorm.
 * User: Matanya
 * Date: 16-Oct-17
 * Time: 16:28
 */

namespace App\Notification;


use Berkayk\OneSignal\OneSignalClient;

class MyOneSignalClient extends OneSignalClient
{
	private $myAppId;
	private $myRestApiKey;


	/**
	 * MyOneSignalClient constructor.
	 */
	public function __construct()
	{
		$this->myAppId = "1d17fc67-d08c-4a3c-a766-ecbffc50b36a";
		$this->myRestApiKey = "N2E0MjAwOGYtZmNlNy00NTNiLWFhOTEtYWQ3NjcyMDFiZmJj";
		parent::__construct($this->myAppId, $this->myRestApiKey, 'OTA3YjdiNWMtMzNiOS00MjFiLTgyY2MtYmFhYjA5MzVlOTI5');
	}

	public function sendNotificationToUsers($message, array $userId, $url = null, $title = null, $data = null, $buttons = null, $schedule = null)
	{
		$contents = array(
			"en" => $message
		);

		$params = array(
			'app_id' => $this->myAppId,
			'contents' => $contents,
			'include_player_ids' => $userId
		);

		if ($url) {
			$params['url'] = $url;
		}
		if ($title) {
			$params['headings'] = array(
				"en" => $title
			);;
		}

		if ($data) {
			$params['data'] = $data;
		}

		if ($buttons) {
			$params['buttons'] = $buttons;
		}

		if ($schedule) {
			$params['send_after'] = $schedule;
		}

		$this->sendNotificationCustom($params);

		return ["success" => true];
	}
}
