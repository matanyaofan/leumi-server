<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TextMail extends Mailable
{
    use Queueable, SerializesModels;
    public $title;
    public $message;

    /**
     * Create a new message instance.
     *
     * @param $title
     * @param $message
     */
    public function __construct($title,$message)
    {
        //
        $this->title = $title;
        $this->subject($title);
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.textmail');
    }
}
