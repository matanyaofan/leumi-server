<?php
/**
 * Created by PhpStorm.
 * User: Lior
 * Date: 12/12/2018
 * Time: 10:02
 */

namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookOrder extends Mailable
{
	use Queueable, SerializesModels;

	protected $address;
	protected $book_name;
	protected $user_name;
	protected $email;
	protected $phone;
	protected $leumi_place;
	protected $date;


	/**
	 * Create a new message instance.
	 *
	 * @param $order
	 * @param $book_name
	 * @param $user
	 */
	public function __construct($order, $book_name, $user)
	{
		$this->user_name = $user->name;
		$this->email = $user->email;
		$this->phone = $user->phone_number;
		$this->leumi_place = $user->leumi_place;
		$this->address = $order->address;
		$this->book_name = $book_name;

		$this->date = mb_convert_encoding(
			date("H:i:s") . " " . jdtojewish(unixtojd(), true, CAL_JEWISH_ADD_GERESHAYIM + CAL_JEWISH_ADD_ALAFIM_GERESH),
			"UTF-8",
			"ISO-8859-8"
		);

		if (!$this->phone) {
			$this->phone = "[אין מספר]";
		}
		if (!$this->leumi_place) {
			$this->leumi_place = "[לא הוזן מקום שירות]";
		}

	}

	public function build()
	{
		return $this->view('mail.bookOrder')
			->subject("הזמנת ספר חדשה מאפליקצית תתהלל ")
			->from("projects@clap.co.il", "אפליקצית תתהלל")//TODO replace mail address
			->with([
				"name" => $this->user_name,
				"email" => $this->email,
				"date" => $this->date,
				"phone" => $this->phone,
				"book" => $this->book_name,
				"address" => $this->address,
				"leumi_place" => $this->leumi_place
			]);
	}
}