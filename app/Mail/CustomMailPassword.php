<?php
/**
 * Created by PhpStorm.
 * User: Lior
 * Date: 18/10/2018
 * Time: 10:41
 */

namespace App\Mail;


use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;

class CustomMailPassword extends ResetPassword
{

	public function toMail($notifiable)
	{
		return (new MailMessage)
			->line('אימייל זה נשלח אליך כיוון שקיבלנו בקשה לאיפוס סיסמא')
			->action('איפוס סיסמא', url(config('app.url') . route('password.reset', $this->token, false)))
			->line('אם לא ביקשת לאפס את הסיסמא- אינך נדרש לעשות דבר.')
			->line('אם המקרה חוזר על עצמו אנא פנה אלינו');
	}
}