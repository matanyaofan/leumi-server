<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 31 דצמ 18
 * Time: 11:03
 */

namespace App\Mail;


use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPasswordNotification extends ResetPassword
{

	/**
	 * ResetPasswordNotification constructor.
	 * @param string $token
	 */
	public function __construct(string $token)
	{
		parent::__construct($token);
	}


	/**
	 * Build the mail representation of the notification.
	 *
	 * @param  mixed $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable)
	{
		return (new MailMessage)
			->subject('שחזור סיסמא- מערכת תתהלל')
			->greeting("שלום")
			->line('את מקבלת את הודעת האימייל הזו מפני שקיבלנו בקשה לאיפוס סיסמה עבור החשבון שלך.')
			->action('איפוס סיסמא', url(config('app.url') . route('password.reset', $this->token, false)))
			->line('אם לא ביקשת איפוס סיסמה, לא נדרשת פעולה נוספת.');
	}
}