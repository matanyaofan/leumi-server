<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;


class Contact extends Mailable
{
	use Queueable, SerializesModels;

	public $name;
	public $email;
	public $text;
	public $date;
	public $phone;
	public $subject1;
	public $leumi_place;

	/**
	 * Create a new message instance.
	 *
	 * @param $request
	 * @param $user
	 */
	public function __construct($request, $user)
	{
		$this->name = $user->name;
		$this->email = $user->email;
		$this->phone = $user->phone_number;
		$this->leumi_place = $user->leumi_place;
		$this->text = $request->input("text");
		$this->subject1 = $request->input("subject");
		$this->date = mb_convert_encoding(
			date("H:i:s") . " " . jdtojewish(unixtojd(), true, CAL_JEWISH_ADD_GERESHAYIM + CAL_JEWISH_ADD_ALAFIM_GERESH),
			"UTF-8",
			"ISO-8859-8"
		);

		if (!$this->phone) {
			$this->phone = "[אין מספר]";
		}
		if (!$this->leumi_place) {
			$this->leumi_place = "[לא הוזן מקום שירות]";
		}
		if (!$this->subject1) {
			$this->subject1 = "[ללא נושא]";
		}
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->view('mail.contact')
			->subject("פנית צור קשר מאפליקציה: " . $this->subject1)
			->from("projects@clap.co.il", "אפליקצית תתהלל")//TODO replace mail address
			->with([
				"name" => $this->name,
				"email" => $this->email,
				"date" => $this->date,
				"phone" => $this->phone,
				"subject1" => $this->subject1,
				"text" => $this->text,
				"leumi_place" => $this->leumi_place
			]);
	}
}
