<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\BookOrder
 *
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BookOrder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BookOrder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BookOrder query()
 * @mixin \Eloquent
 */
class BookOrder extends Model
{
	protected $table = "book_orders";
	protected $fillable = ['user_id', 'book_id', 'is_reach', 'address'];

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function book(){
	    return $this->belongsTo('App\Book');
    }


    protected $casts = [
        "is_reach"=>"boolean"
    ];
}
