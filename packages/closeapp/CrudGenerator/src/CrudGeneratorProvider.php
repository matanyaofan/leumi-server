<?php

namespace Closeapp\CrudGenerator;



use Illuminate\Support\ServiceProvider;

class CrudGeneratorProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->commands(CrudGeneratorCommand::class);
	}


	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

}
