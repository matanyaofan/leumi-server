<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 11/06/18
 * Time: 12:13
 */
?>
<!--suppress ALL -->
<template>
	<v-select :options="options" v-model="model" label="name"></v-select>
</template>

<script>
	import {{$modelName}}Module from "dev/store/modules/{{$modelName}}.module";
	import SelectMixin from "dev/mixins/SelectMixin";

	export default {
		name: "{{$modelNamePluralLowerCase}}-select",
		props: {value: {default: null}},
		components: {},
		mixins: [new SelectMixin({{$modelName}}Module, "{{$modelNamePluralLowerCase}}")],
	};
</script>
