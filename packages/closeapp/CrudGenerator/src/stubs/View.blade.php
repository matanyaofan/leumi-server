<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 11/06/18
 * Time: 12:13
 */

use Closeapp\CrudGenerator\helper\StringManipulation;

$sm = StringManipulation::create($modelName);

?>

<template>

	<page>
		<title-view>מסך {{$modelNamePluralLowerCase}}
			<v-btn slot="actions" flat @click="openModal = true">add {{$modelName}}</v-btn>
		</title-view>
		<row>
			<c-col>



			</c-col>
			<c-col>
				<{{$modelNamePluralLowerCase}}-table @edit="onEdit"/>
			</c-col>
		</row>
		<popup-dialog v-model="openModal">
<span slot="header">{{$sm->getModelName("","יצירת/עריכת ")}}</span>
			<{{$modelNameSingularLowerCase}}-form @save="openModal=false"/>
		</popup-dialog>

	</page>


</template>

<script>
	import {{$sm->getModelName("Form")}} from "dev/view/{{$sm->getModelNamePluralLowerCase()}}/{{$sm->getModelName("Form")}}";
	import {{$sm->getModelNamePlural("Table")}} from "dev/view/{{$sm->getModelNamePluralLowerCase()}}/{{$sm->getModelNamePlural("Table")}}";

	export default {
		name: "{{$modelNamePluralLowerCase}}-view",
		components: {{"{".$modelNamePlural}}Table, {{$modelName}}Form},
		data() {
			return {
				openModal: false
			};
		},
		methods: {
			onEdit({{$modelName}}) {
				this.$store.commit(StudentModule.types.GET, {{$modelName}});
				this.openModal = true;
			}
		}

	};
</script>

<style scoped>

</style>