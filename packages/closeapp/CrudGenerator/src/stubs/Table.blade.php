<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 11/06/18
 * Time: 12:13
 */
?>

<template>
	<row>
		<c-col>
			<v-card>
				<v-card-title>
					<v-spacer></v-spacer>
					<v-text-field
							v-model="searchText"
							append-icon="search"
							label="Search"
							single-line
							hide-details
							right
					></v-text-field>
				</v-card-title>
	<v-data-table
			:headers="headers"
			:items="{{$modelNamePluralLowerCase}}"
			:loading="loading"
			class="elevation-1"
	>
		<v-progress-linear slot="progress" color="blue" indeterminate></v-progress-linear>
		<{{$modelNameSingularLowerCase}}-row slot="items" slot-scope="props" @edit="onEdit"   :{{$modelNameSingularLowerCase}}="props.item"/>
	</v-data-table>
		</c-col>
	</row>
</template>

<script>
	import {{$modelName}}Row from "dev/view/{{$modelNamePluralLowerCase}}/{{$modelName}}Row";
	import {types} from "dev/store/modules/{{$modelName}}.module";
	import FilterAble from "dev/mixins/FilterAble";

	export default {
		name: "{{$modelNamePluralLowerCase}}-table",
		props: [],
		components: {
			{{$modelName}}Row
		},
		mixins: [new FilterAble(types)],
		data() {
			return {loading: false,
				headers: [
			@foreach($validations as $key => $validation)
					{text:"{{$key}}",value:"{{$key}}" },
				@endforeach

				],
				search: null,
				showDialog: false};
		},
		computed: {
			{{$modelNamePluralLowerCase}}() {
				return this.$store.state.{{$modelName}}Module.{{$modelNamePluralLowerCase}};
			}
		},
		methods: {
			onEdit({{$modelName}}) {
				this.$emit("edit", {{$modelName}})
			}
		}
	};
</script>

<style>

</style>