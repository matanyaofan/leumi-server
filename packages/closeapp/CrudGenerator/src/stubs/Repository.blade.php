<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 11/06/18
 * Time: 12:13
 */

/**

 **/

use Closeapp\CrudGenerator\helper\StringManipulation;

$sm = StringManipulation::create($modelName);
?>

namespace App\Repositories\{{$sm->getModelNamePlural()}};

use Closeapp\Repository\Repository;


class {{$modelName}}Repository extends Repository
{

	public function find($id)
	{
		return (new {{$sm->getModelName("","Search()")}})->find($id);
	}

	public function all($filters = [])
	{
		return (new {{$sm->getModelName("","Search()")}})->setFilters($filters)->getQuery()->get();
	}
}
