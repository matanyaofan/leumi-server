<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 11/06/18
 * Time: 12:13
 */

use Closeapp\CrudGenerator\helper\StringManipulation;

$sm = StringManipulation::create($modelName);
?>

<!--suppress ALL -->
<!--suppress UnreachableCodeJS -->
<template>

	<form @submit.prevent="onSubmit">

		@foreach($validations as $key => $validation)
			<smart-input :error="inputErrors.{{$key}}" label='{{$key}}' v-model="form.{{$key}}"></smart-input>
		@endforeach
			<div class="form-group">
				<v-btn type="submit" color="success">שמירה</v-btn>
			</div>
	</form>
</template>


<script>
	import {{$sm->getModelName("Module")}} from "dev/store/modules/{{$modelName}}.module";
	import FormView from "dev/mixins/FormView";
	export default {
		props: [],
		name: "{{$modelNameSingularLowerCase}}-form",
		components: {},
		data() {
			return {
				form: {
					name: null,
				},
			};
		},
		mixins: [new FormView({{$sm->getModelName("Module")}},"{{$sm->getModelNameSingularLowerCase()}}")],
		computed: {
			isNew(){
				return !!this.{{$modelNameSingularLowerCase}}.id;
			}
		}
	};
</script>

<style>

</style>