<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 11/07/18
 * Time: 08:51
 */

namespace Closeapp\CrudGenerator\Generators;


use Closeapp\CrudGenerator\helper\DataManager;

class RepositoryGenerator extends Generator
{


	/**
	 * @var DataManager
	 */
	private $dataManager;

	public function __construct(DataManager $dataManager)
	{
		$this->dataManager = $dataManager;
	}

	public function create(): string
	{
		return self::getStub("Repository", $this->dataManager->getOptions(), true);
	}


	public function getPath(): string
	{
		return app_path("/Repositories/{$this->dataManager->modelNamePlural()}/{$this->dataManager->getModelName()}Repository.php");
	}
}