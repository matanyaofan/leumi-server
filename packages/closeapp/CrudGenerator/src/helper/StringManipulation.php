<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 03/12/18
 * Time: 10:39
 */

namespace Closeapp\CrudGenerator\helper;


class StringManipulation
{
	private $modelName;

	public function __construct($modelName)
	{
		$this->modelName = $modelName;
	}

	/**
	 * @return string
	 */
	public function getModelName($endWith = null, $startWith = null)
	{
		return $startWith . $this->modelName . $endWith;
	}

	/**
	 * @param mixed $modelName
	 */
	public function setModelName($modelName): void
	{
		$this->modelName = $modelName . $endWith;
	}


	/**
	 * @return string
	 */
	public function getModelNameSingularLowerCase($endWith = null, $startWith = null)
	{
		return $startWith . strtolower($this->modelName) . $endWith;
	}

	/**
	 * @return string
	 */
	public function getModelNamePluralLowerCase($endWith = null, $startWith = null)
	{
		return $startWith . strtolower($this->getModelNamePlural()) . $endWith;
	}

	/**
	 * @return string
	 */
	public function getModelNamePlural($endWith = null, $startWith = null)
	{
		return $startWith . str_plural($this->modelName) . $endWith;
	}


	public function getAllManipulation()
	{
		return [
			"modelName" => $this->getModelName(),
			"modelNamePlural" => $this->getModelNamePlural(),
			"modelNamePluralLowerCase" => $this->getModelNamePluralLowerCase(),
			"modelNameSingularLowerCase" => $this->getModelNameSingularLowerCase()
		];
	}


	public static function create($modelName)
	{
		return new self($modelName);
	}
}