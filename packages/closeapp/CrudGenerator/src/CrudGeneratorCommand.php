<?php

namespace Closeapp\CrudGenerator;

use Closeapp\CrudGenerator\Generators\RepositoryGenerator;
use Closeapp\CrudGenerator\Generators\SearchGenerator;
use Closeapp\CrudGenerator\helper\ColumnHelper;
use Closeapp\CrudGenerator\helper\DataManager;
use Closeapp\CrudGenerator\helper\StringManipulation;
use File;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use View;
use function app_path;
use function file_exists;
use const PHP_EOL;

class CrudGeneratorCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'crud:generator {name : Class (singular) for example User} {--namespace=} {--only=*} {--F|force}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create CRUD operations';

	/**
	 * @var Model
	 */
	private $model;
	private $force;
	/**
	 * @var DataManager
	 */
	private $dataManager;
	private $namespace;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 */
	public function handle()
	{
		$name = $this->argument('name');
		$modelName = "\App\\" . $this->argument('name');
		$this->model = new $modelName;
		$this->dataManager = new DataManager($name);
		$options = $this->getNameOptions($name);
		$options["columnsHelper"] = ColumnHelper::getTableColumns($this->model->getTable());;
		$options["validations"] = $this->getValidations($options["columnsHelper"]);
		$options["columns"] = \DB::getSchemaBuilder()->getColumnListing($this->model->getTable());
		$this->namespace = $this->option("namespace") ? $this->option("namespace") . "/" : "";
		$options["namespace"] = $this->namespace;
		$this->force = $this->option("force");

		$actions = ["controller", "request", "service", "module", "views", "repository", "search"];
		$only = $this->option("only");
		if (!empty($only)) {
			$actions = $only;
		}

		foreach ($actions as $action) {
			$this->$action($name, $options);
		}

		echo "done";
	}


	protected function getStub($type, $data = [], $addPHP = true)
	{
		return ($addPHP ? "<?php" . PHP_EOL : "") . View::file(__DIR__ . "\\stubs\\$type.blade.php", $data)->render();
	}


	protected function search($name, $options)
	{
		$generator = new SearchGenerator($this->dataManager);
		$this->createFile($generator->getPath(), $generator->create());
	}


	protected function repository($name, $options)
	{
		$generator = new RepositoryGenerator($this->dataManager);
		$this->checkDirectoryExist(app_path("Repositories"));
		$this->checkDirectoryExist(app_path("Repositories/{$this->dataManager->modelNamePlural()}"));
		$this->createFile($generator->getPath(), $generator->create());
	}

	protected function controller($name, $options)
	{
		$controllerTemplate = $this->getStub('Controller', $options);
		$this->createFile(app_path("/Http/Controllers/{$this->namespace}{$name}Controller.php"), $controllerTemplate);
	}

	protected function service($name, $options)
	{
		$template = $this->getStub('Service', $options, false);
		$this->createFile("dev/services/{$name}Service.js", $template);
	}

	protected function module($name, $options)
	{
		$template = $this->getStub('Module', $options, false);
		$this->createFile("dev/store/modules/{$name}.module.js", $template);
	}

	protected function request($name, $options)
	{
		$requestTemplate = $this->getStub('Request', $options);
		if (!file_exists($path = app_path('/Http/Requests')))
			mkdir($path, 0777, true);

		$filePath = app_path("/Http/Requests/{$name}Request.php");
		$this->createFile($filePath, $requestTemplate);

	}

	private function createFile($path, $contents)
	{
		if (!file_exists($path) || $this->force) {
			file_put_contents($path, $contents);
			echo "create $path " . PHP_EOL;
		} else {
			echo "file exists!!!  $path" . PHP_EOL;
		}
	}

	private function getNameOptions(string $name): array
	{
		return StringManipulation::create($name)->getAllManipulation();
	}

	private function getValidations(array $columns)
	{

		$validates = [];
		/** @var ColumnHelper $column */
		foreach ($columns as $column) {
			if (in_array($column->getField(), ["id", "updated_at", "created_at", "deleted_at", "remember_token"])) {
				continue;
			}
			$validates[$column->getField()] = $column->getValidate();
		}
		return $validates;
	}

	protected function views($name, $options)
	{

		if (!file_exists($path = "dev/view/" . $this->namespace . $options["modelNamePluralLowerCase"]))
			mkdir($path, 0777, true);
		$this->modelView($options, $path);
		$this->modelForm($name, $options, $path);
		$this->modelTable($options, $path);
		$this->modelRow($name, $options, $path);
		$this->modelSelect($options, $path);
	}

	private function modelView($options, $path)
	{
		$template = $this->getStub('View', $options, false);
		$this->createFile("{$path}/{$options["modelNamePlural"]}View.vue", $template);
	}

	private function modelForm($name, $options, $path)
	{
		$template = $this->getStub('Form', $options, false);
		$this->createFile("{$path}/{$name}Form.vue", $template);
	}

	private function modelTable($options, $path)
	{
		$template = $this->getStub('Table', $options, false);
		$this->createFile("{$path}/{$options["modelNamePlural"]}Table.vue", $template);
	}

	private function modelRow($name, $options, $path)
	{
		$template = $this->getStub('Row', $options, false);
		$this->createFile("{$path}/{$name}Row.vue", $template);
	}

	private function modelSelect($options, $path)
	{
		$template = $this->getStub('Select', $options, false);
		$this->createFile("{$path}/{$options["modelNamePlural"]}Select.vue", $template);
	}

	private function checkDirectoryExist(string $path)
	{
		if (!File::isDirectory($path)) {
			File::makeDirectory($path);
		}
	}

}
