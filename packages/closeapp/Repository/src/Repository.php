<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 02 ינו 19
 * Time: 09:54
 */

namespace Closeapp\Repository;


abstract class Repository
{

	abstract public function find($id);

	abstract public function all();
}