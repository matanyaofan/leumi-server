<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 27/06/18
 * Time: 12:31
 */

namespace Closeapp\Repository;


use Closeapp\JoinQuery\Join;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Str;
use ReflectionException;
use function collect;
use function str_replace;

abstract class Search
{
	/**
	 * @var \App\Builder\Query|\Illuminate\Database\Eloquent\Builder
	 */
	protected $query;
	/**
	 * @var string
	 */
	protected $namespace = "\\App\\";

	/**
	 * @var string|null
	 */
	protected $baseScope = null;

	/**
	 * Search constructor.
	 * @param bool $withRequestFilters
	 */
	public function __construct()
	{

		$className = $this->getBaseScopeName();

		$this->query = $className::query();
//		$this->withRequestFilters($withRequestFilters);
	}

	/**
	 * @return \App\Builder\Query|\Illuminate\Database\Eloquent\Builder
	 */
	public function getQuery()
	{
		return $this->query;
	}

	public function whereId($id)
	{
		$this->query->where("id", $id);
		return $this;
	}


	/**
	 * @param array $columns
	 * @return \App\Builder\Query|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
	 */
	public function first($columns = ['*'])
	{
		return $this->query->first($columns);
	}

	/**
	 * @param array $columns
	 * @return \App\Builder\Query|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed|null
	 */
	public function find($id, $columns = ['*'])
	{
		return $this->query->find($id, $columns);
	}


	/**
	 * @param array $columns
	 * @return $this
	 */
	public function select($columns = ['*'])
	{
		$this->getQuery()->select($columns);
		return $this;
	}

	/**
	 * @param $className
	 * @param $attributes
	 * @param $type
	 * @return Search
	 * @throws \Exception
	 */
	public function joinBelongsTo($className, array $attributes = [], $type = "left")
	{
		Join::asBelongsTo($this->getQuery(), $className, $attributes, $type);
		return $this;
	}

	/**
	 * @param $className
	 * @param array $attributes
	 * @param string $type
	 * @return $this
	 * @throws \Exception
	 */
	public function joinHasOne($className, array $attributes = [], $type = "left")
	{
		Join::asHasOne($this->getQuery(), $className, $attributes, $type);
		return $this;
	}

	/**
	 * @param $query
	 * @param $as
	 * @param $first
	 * @param $operator
	 * @param $second
	 * @param string $type
	 * @return $this
	 */
	public function joinQuery($query, $as, $first, $operator, $second, $type = "inner")
	{
		Join::joinQuery($this->getQuery(), $query, $as, $first, $operator, $second, $type);
		return $this;
	}

	/**
	 * @param array $filters
	 * @return $this
	 */
	public function setFilters(array $filters)
	{
		$this->applyFilter($filters);
		return $this;
	}

	private function applyFilter(array $filters)
	{
		$filtersCollect = collect($filters);
		if ($filtersCollect->has("orderBy")) {
			$reverse = $filtersCollect->get("reverse", 0) ? "desc" : "asc";
			$this->query->orderBy($filtersCollect->get("orderBy"), $reverse);
		}
		$filtersCollect->each(function ($value, $key) {
			$options = ["", "where_"];
			foreach ($options as $option) {
				$functionName = Str::camel($option . $key);
				if (method_exists($this, $functionName)) {
					return $this->$functionName($value);
				}
			}

		});
	}


	public static function fromQuery(Builder $query)
	{
		$search = new static();
		$table = $search->query->getModel()->getTable();
		$query->from($table);
		$search->query = $query;
		return $search;
	}

	public function getBaseScopeName()
	{
		if ($this->baseScope) {
			return $this->namespace . $this->baseScope;
		}
		try {
			$reflectionClass = new \ReflectionClass(static::class);
		} catch (ReflectionException $e) {

		}
		return $this->namespace . str_replace("Search", "", $reflectionClass->getShortName());
	}


}