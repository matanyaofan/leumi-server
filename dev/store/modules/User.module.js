import UserService from "dev/services/UserService";
import ModuleCreator from "dev/store/ModuleCreator";

export const types = {};
export const params = {
	email: "",
	name: "",
	role: "",
	password: "",
	leumi_place: "",
	phone_number: "",
	organization: "",
	OS_id: "",
	daily_reminder: "",
	study_reminder: "",
	image: "",
	current_book_number: "",
	tz: "",
	city: "",
	street: "",
	home_number: "",
	ZIP_code: "",

};

/**
 @namespace
 @property  {object} module,
 @property  {object} module.state,
 @property  {object} modules.user,
 @property  {array}  modules.users
 **/

const module = {
	state: {},
	mutations: {},
	getters: {},
	actions: {},
	types: types
};
export default ModuleCreator("user", UserService, module, types, params);