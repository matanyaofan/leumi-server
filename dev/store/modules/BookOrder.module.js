import BookOrderService from "dev/services/BookOrderService";
import ModuleCreator from "dev/store/ModuleCreator";


export const types = {};
export const params = {
	book_id:"",
	user_id:"",
	address:"",
	is_reach:"",

};

/**
@namespace
@property  {object} module,
@property  {object} module.state,
@property  {object} modules.bookorder,
@property  {array}  modules.bookorders
**/

const module = {
	state: {},
	mutations: {},
	getters: {},
	actions: {},
	types: types
};
export default ModuleCreator("bookorder", BookOrderService, module, types ,params);