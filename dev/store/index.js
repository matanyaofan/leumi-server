import Vue from "vue";
import Vuex from "vuex";
import AuthModule from "./modules/Auth.module";
import UserModule from "./modules/User.module";
import BookOrderModule from "./modules/BookOrder.module";

Vue.use(Vuex);
const store = new Vuex.Store({
	state: {},
	modules: {
		AuthModule,
		UserModule,
		BookOrderModule
	}
});
export default store;
