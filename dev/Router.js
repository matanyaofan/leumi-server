/**
 * Created by elad on 15 נובמבר 2016.
 */
import VueRouter from "vue-router";
import middleware from "dev/middleware";
import LoginView from "dev/view/auth/LoginView";
import UsersView from "dev/view/users/UsersView";
import WelcomeView from "dev/view/WelcomeView";
import NotificationView from "dev/view/notifications/NotificationView";
import BookOrdersView from "dev/view/bookorders/BookOrdersView";

const routes = [
	{path: "/", component: WelcomeView,redirect:{name: "users-page"} , name: "home-page", meta: {"RequireAuth": true}},
	{path: "/students", component: UsersView, name: "users-page", meta: {"RequireAuth": true}},
	{path: "/notifications", component: NotificationView, name: "notifications-page", meta: {"RequireAuth": true}},
	{path: "/login", name: "login-page", component: LoginView, meta: {"freeAccess": true}},
	{path: "/bookorders", name: "bookorders-view", component: BookOrdersView, meta:  {"RequireAuth": true}}
];
const router = new VueRouter({
	history: false,
	mode: "history",
	routes
});


router.beforeEach((to, from, next) => {
	middleware.check(to, from, next);
});


export default router;
