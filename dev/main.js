// import 'bootstrap';

import router from "dev/Router";
import {sync} from "vuex-router-sync";
import Notification from "dev/helpers/Notification";
import Vue from "vue";
import VueRouter from "vue-router";
import store from "dev/store";
import {setRouter} from "dev/packages/MyAxios";
import "dev/scaffoldings/vuetify";
import "izitoast/dist/css/iziToast.css";
import App from "dev/App";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";

Vue.use(Vuetify, {
	rtl: true
});
Vue.use(VueRouter);
sync(store, router);
Vue.use(Notification);
setRouter(router);
moment.locale("he");
new Vue({
	store,
	router,
	render: (ce) => {
		return ce(App);
	},
	components: {},
}).$mount("#app");
