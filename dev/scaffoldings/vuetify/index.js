import Vue from "vue";
import PageView from "./PageView";
import Row from "./Row";
import cCol from "./cCol";
import TitleView from "./TitleView";
import BlockLine from "./BlockLine";
import SmartInput from "./SmartInput";
import MessageBox from "./MessageBox";
import PopupDialog from "./PopupDialog";

export default {
	PageView,
	Row,
	BlockLine,
	TitleView,
	cCol,
	SmartInput,
	MessageBox
};

Vue.component("page", PageView);
Vue.component("row", Row);
Vue.component("title-view", TitleView);
Vue.component("block-line", BlockLine);
Vue.component("smart-input", SmartInput);
Vue.component("c-col", cCol);
Vue.component("message-box", MessageBox);
Vue.component("popup-dialog", PopupDialog);