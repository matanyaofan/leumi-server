import Vue from "vue";
import PageView from "./PageView";
import Row from "./Row";
import TitleView from "./TitleView";
import BlockLine from "./BlockLine";

export default {
	PageView,
	Row,
	BlockLine,
	TitleView
};

Vue.component("page", PageView);
Vue.component("row", Row);
Vue.component("view-title", TitleView);