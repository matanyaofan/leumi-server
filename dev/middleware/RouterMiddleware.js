class RouterMiddleware {


	constructor(middlewares) {
		this.middlewares = middlewares;
	}

	check(to, from, next) {
		let index = 0;
		this.runMiddleware(index, to, from, next);
	}

	async runMiddleware(index, to, from, next) {
		const metaArray = Object.keys(to.meta);
		if (metaArray[index]) {
			let middlewareName = metaArray[index];
			if (this.middlewares[middlewareName]) {
				let middleware = new this.middlewares[middlewareName];
				await middleware.handle(to, from, (obj) => {
					if (obj) {
						next(obj);
					} else {
						index++;
						if (metaArray.length > index) {
							this.runMiddleware(index, to, from, next);
						} else {
							next();
						}
					}
				}, to.meta[middlewareName]);
			} else {
				index++;
				if (metaArray.length > index) {
					this.runMiddleware(index, to, from, next);
				} else {
					next();
				}
			}
		} else {
			next();
		}
	}

	generateNextFun() {
		return (obj) => {
			if (obj) {

			}
		};
	}


}

export default RouterMiddleware;