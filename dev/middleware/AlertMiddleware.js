import BaseMiddleware from "dev/middleware/BaseMiddleware";

class AuthMiddleware extends BaseMiddleware {
	handle(from, to, next, args) {
		return next();
	}
}

export default AuthMiddleware;