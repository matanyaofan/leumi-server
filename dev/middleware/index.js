import RouterMiddleware from "dev/middleware/RouterMiddleware";
import AuthMiddleware from "dev/middleware/AuthMiddleware";
import AlertMiddleware from "dev/middleware/AlertMiddleware";
import GuestMiddleware from "dev/middleware/GuestMiddleware";


export default new RouterMiddleware({
	"RequireAuth": AuthMiddleware,
	"auth": AuthMiddleware,
	"guest": GuestMiddleware,
	"alert": AlertMiddleware,
});
