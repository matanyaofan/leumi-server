import BaseMiddleware from "dev/middleware/BaseMiddleware";
import {types as authType} from "dev/store/modules/Auth.module";
import store from "dev/store";

class AuthMiddleware extends BaseMiddleware {


	static goToLogin(next) {
		return next({
			name: "login-page",
		});
	}

	async handle(from, to, next, args) {
		if (!store.getters.isConnected) {
			try {
				await store.dispatch(authType.GET);
				if (!store.getters.isConnected) {
					return AuthMiddleware.goToLogin(next);
				} else {
					return next();
				}
			} catch (e) {
				return AuthMiddleware.goToLogin(next);
			}
		} else {
			return next();
		}
	}
}

export const rules = {};
AuthMiddleware.rules = {
	MEMBER: "member",
	ADMIN: "admin",
	MANAGER: "manager"
};

export default AuthMiddleware;