const FilterAble = (types) => {
	return {
		data: function () {
			return {
				filters: {
					page: 1,
				},
				settings: {},
				loading: false,
			};
		},
		methods: {
			changePage(page) {
				this.filters.page = page;
				this.updateQuery();
				this.getData();
			},
			order({orderBy, reverse}) {
				this.filters.orderBy = orderBy;
				this.filters.reverse = reverse;
				this.filters.page = 1;
				this.updateQuery();
				this.getData();
			},
			filter({filterName, filterValue}) {
				this.filters[filterName] = filterValue;
				this.filters.page = 1;
				this.updateQuery();
				this.getData();
			},
			async getData() {
				this.updateQuery();
				this.loading = true;
				let response;
				if (this.$notfication && this.$notfication.loading) {
					response = await ethis.$notfication.loading(this.getDataFromServer());
				} else {
					response = await this.getDataFromServer();
				}
				this.loading = false;
				return response;
			},
			getDataFromServer() {
				return this.$store.dispatch(types.FETCH, this.filters).then((response) => {
					if (!Array.isArray(response)) {
						this.settings = response;
					}
					return response;
				});
			},
			updateQuery() {
				this.$router.replace({
					query: {}
				});
				this.$router.replace({
					query: Object.assign({}, this.filters)
				});
			}
		},
		created: function () {
			this.filters = Object.assign(this.filters, this.$route.query);
			this.getData();
		}
	};
};
export default FilterAble;