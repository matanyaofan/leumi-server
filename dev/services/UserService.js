import BaseService from "dev/packages/BaseService";

export default class UserLessonService extends BaseService {
	static get className() {
		return "user";
	}
};