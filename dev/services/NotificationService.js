import BaseService from "dev/packages/BaseService";

export default class NotificationService extends BaseService {
	static get className() {
		return "notification";
	}
};