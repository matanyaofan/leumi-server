<?php

namespace Tests\Feature;

use Tests\TestCase;

class NotificationTest extends TestCase
{
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testExample()
	{
		$this->withoutMiddleware();
		$request = $this->post("api/notification", ["message" => "hello", "title" => "title"]);
		$request->assertOk();
	}
}
