<?php
/**
 * Created by PhpStorm.
 * User: Lior
 * Date: 11/03/2019
 * Time: 11:56
 */

return [
	"Regards" => ",בברכה",
	"If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\ninto your web browser: [:actionURL](:actionURL)" => "אם אינך מצליח ללחוץ על הכפתור \":actionText\" העתיקי את הכתובת  URL לדפדפן שלך \n[:actionURL](:actionURL)",
	"All rights reserved" => "כל הזכויות שמורות"
];