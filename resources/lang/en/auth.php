<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Authentication Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used during authentication for various
	| messages that we need to display to the user. You are free to modify
	| these language lines according to your application's requirements.
	|
	*/

	'failed' => 'הנתונים שהכנסת אינם תואמים את רישומנו.',
	'throttle' => 'יותר מידי נסיונות החברות. נסי שוב בעוד :seconds שניות.',

];
