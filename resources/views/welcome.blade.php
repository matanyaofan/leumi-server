<!doctype html>
<html lang="he">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{config("app.name")}}</title>



    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<script>
		window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
	</script>
</head>
<body>
<div id="app">

</div>
@if(App::environment('production'))
    <script src="/js/vendor.js?date={{\File::lastModified("js/vendor.js")}}"></script>
    <script src="/js/prod.js?date={{\File::lastModified("js/prod.js")}}"></script>
@else
    <script src="/js/main.js"></script>
@endif
</body>
</html>
