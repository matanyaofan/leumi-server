<?php
/**
 * Created by PhpStorm.
 * User: Matanya
 * Date: 14-Nov-17
 * Time: 10:10
 */
?>

{{--<!DOCTYPE html>--}}
<html lang="en">
<head>
    <style>
        .main {
            font-size: x-large;
            padding: 50px 0;
        }

        h1 {
            text-align: center;
        }
    </style>
</head>
<body dir="rtl">
<div style="padding: 5%">
    <h1>שאל את הרב - {{$subject1}}</h1>
    <p><b>שאלה שהתקבלה מאת: </b>{{$name}}</p>
    <p><b>בתאריך: </b>{{$date}}</p>
    <p class="main">
        <b>תוכן השאלה: </b>{{$text}}
    </p>
    <p><b>כתובת למשלוח התשובה: </b><a href="mailto:{{$email}}" target="_blank">{{$email}}</a></p>
    <p><b>טלפון ליצירת קשר: </b>{{$phone}}</p>
    <p><b>מקום השירות: </b>{{$leumi_place}}</p>
</div>
</body>