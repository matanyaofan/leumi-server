@component('mail::message')

#{{$title}}

{{$message}}

בברכה<br>
{{ config('app.name') }}
@endcomponent
